<?php

return [
   'catalog' => [
      'pim' => [
         'base_uri' => env('PIM_SERVICE_HOST') . "/api/v1",
      ],
   ],
];
