<?php

use App\Domain\Contents\Models\BannerType;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $model = new BannerType();
        $model->code = BannerType::CATALOG_ITEM_CODE;
        $model->name = 'В каталоге среди товаров';
        $model->active = true;
        $model->save();

        $model = new BannerType();
        $model->code = BannerType::CATALOG_TOP_CODE;
        $model->name = 'В каталоге сверху';
        $model->active = true;
        $model->save();

        $model = new BannerType();
        $model->code = BannerType::PRODUCT_GROUP_CODE;
        $model->name = 'На продуктовой странице';
        $model->active = true;
        $model->save();

        $model = new BannerType();
        $model->code = BannerType::WIDGET_CODE;
        $model->name = 'В виджете';
        $model->active = true;
        $model->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        BannerType::query()->delete();
    }
};
