<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    const PRODUCT_GROUPS_TABLE = 'product_groups';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(self::PRODUCT_GROUPS_TABLE, function (Blueprint $table) {
            $table->boolean('is_shown')->default(true)->after('active');
            ;
            $table->dropColumn('added_in_menu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::PRODUCT_GROUPS_TABLE, function (Blueprint $table) {
            $table->dropColumn('is_shown');
            $table->boolean('added_in_menu')->default(true);
        });
    }
};
