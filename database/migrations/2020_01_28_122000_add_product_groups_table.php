<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_group_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');

            $table->timestamps();
        });

        Schema::create('product_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');
            $table->boolean('active')->default(true);
            $table->boolean('added_in_menu')->default(true); // deprecated
            $table->jsonb('preview_photo')->nullable();
            $table->bigInteger('type_id')->unsigned();
            $table->string('category_code')->nullable();

            $table->timestamps();

            $table->foreign('type_id')->references('id')->on('product_group_types');
        });

        Schema::create('product_group_filters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_group_id')->unsigned();
            $table->string('code');
            $table->string('value');

            $table->timestamps();

            $table->foreign('product_group_id')->references('id')->on('product_groups');
        });

        Schema::create('product_group_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_group_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->integer('sort')->unsigned()->default(500);

            $table->timestamps();

            $table->foreign('product_group_id')->references('id')->on('product_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_group_products');
        Schema::dropIfExists('product_group_filters');
        Schema::dropIfExists('product_groups');
        Schema::dropIfExists('product_group_types');
    }
};
