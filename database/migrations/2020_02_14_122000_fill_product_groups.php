<?php

use App\Domain\Contents\Models\ProductGroupType;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    const TYPES = [
        [
            'name' => 'Акции',
            'code' => ProductGroupType::CODE_PROMO,
        ],
        [
            'name' => 'Подборки',
            'code' => ProductGroupType::CODE_SETS,
        ],
        [
            'name' => 'Бренды',
            'code' => ProductGroupType::CODE_BRANDS,
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (self::TYPES as $type) {
            $model = new ProductGroupType();
            $model->code = $type['code'];
            $model->name = $type['name'];
            $model->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        ProductGroupType::query()->delete();
    }
};
