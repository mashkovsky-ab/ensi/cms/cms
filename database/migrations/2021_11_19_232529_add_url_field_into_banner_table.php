<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    const BANNERS_TABLE = 'banners';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(self::BANNERS_TABLE, function (Blueprint $table) {
            $table->string('url')->nullable()->after('mobile_image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::BANNERS_TABLE, function (Blueprint $table) {
            $table->dropColumn('url');
        });
    }
};
