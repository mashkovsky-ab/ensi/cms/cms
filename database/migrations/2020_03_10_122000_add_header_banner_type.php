<?php

use App\Domain\Contents\Models\BannerType;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $model = new BannerType();
        $model->code = BannerType::HEADER_CODE;
        $model->name = 'В шапке';
        $model->active = true;
        $model->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        BannerType::query()->where('code', BannerType::HEADER_CODE)->delete();
    }
};
