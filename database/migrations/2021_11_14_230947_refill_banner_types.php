<?php

use App\Domain\Contents\Models\BannerType;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    const BANNER_TYPES_TABLE = 'banner_types';
    const BANNERS_TABLE = 'banners';

    const TYPES_OLD = [
        [
            'code' => BannerType::CATALOG_ITEM_CODE,
            'name' => 'В каталоге среди товаров',
        ],
        [
            'code' => BannerType::CATALOG_TOP_CODE,
            'name' => 'В каталоге сверху',
        ],
        [
            'code' => BannerType::PRODUCT_GROUP_CODE,
            'name' => 'На продуктовой странице',
        ],
        [
            'code' => BannerType::WIDGET_CODE,
            'name' => 'В виджете',
        ],
        [
            'code' => BannerType::HEADER_CODE,
            'name' => 'В шапке',
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $model = new BannerType();
        $model->code = BannerType::MAIN_CODE;
        $model->name = 'Главный баннер';
        $model->active = true;
        $model->save();

        DB::table(self::BANNERS_TABLE)->update(['type_id' => $model->id]);

        DB::table(self::BANNER_TYPES_TABLE)
            ->where('id', '!=', $model->id)
            ->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (self::TYPES_OLD as $typeData) {
            $model = new BannerType();
            $model->code = $typeData['code'];
            $model->name = $typeData['name'];
            $model->active = true;
            $model->save();
        }

        /** @var BannerType $substitute */
        $substitute = BannerType::query()
            ->where('code', '!=', BannerType::MAIN_CODE)
            ->firstOrFail();

        DB::table(self::BANNERS_TABLE)->update(['type_id' => $substitute->id]);

        DB::table(self::BANNER_TYPES_TABLE)
            ->where('code', BannerType::MAIN_CODE)
            ->delete();
    }
};
