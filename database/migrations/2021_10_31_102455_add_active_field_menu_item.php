<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    const MENU_ITEMS_TABLE = 'menu_items';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(self::MENU_ITEMS_TABLE, function (Blueprint $table) {
            $table->boolean('active')->default(true)->after('parent_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::MENU_ITEMS_TABLE, function (Blueprint $table) {
            $table->dropColumn('active');
        });
    }
};
