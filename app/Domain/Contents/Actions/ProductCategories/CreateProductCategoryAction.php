<?php

namespace App\Domain\Contents\Actions\ProductCategories;

use App\Domain\Contents\Models\ProductCategory\ProductCategory;

class CreateProductCategoryAction
{
    public function execute(array $fields, array $pimCategories, CreateProductPimCategoryAction $productPimCategoryAction): ProductCategory
    {
        $productCategoryModel = new ProductCategory();
        $productCategoryModel->fill($fields);
        $productCategoryModel->save();

        foreach ($pimCategories as $pimCategory) {
            $pimCategory['product_category_id'] = $productCategoryModel->id;
            $productPimCategoryAction->execute($pimCategory, $pimCategory['filters'] ?? []);
        }

        return $productCategoryModel->load(['pimCategories.filters']);
    }
}
