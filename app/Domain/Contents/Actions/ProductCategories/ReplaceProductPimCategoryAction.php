<?php

namespace App\Domain\Contents\Actions\ProductCategories;

use App\Domain\Contents\Models\ProductCategory\ProductPimCategory;
use App\Domain\Contents\Models\ProductCategory\ProductPimCategoryFilter;

class ReplaceProductPimCategoryAction
{
    public function execute(int $id, array $fields, array $filters): ProductPimCategory
    {
        /** @var ProductPimCategory $productPimCategoryModel */
        $productPimCategoryModel = ProductPimCategory::query()
            ->where('id', $id)
            ->with('filters')
            ->firstOrFail();

        $productPimCategoryModel->fill($fields);
        $productPimCategoryModel->save();

        ProductPimCategoryFilter::destroy($productPimCategoryModel->filters);
        foreach ($filters as $filter) {
            $model = new ProductPimCategoryFilter();
            $model->fill($filter);
            $model->setAttribute('product_pim_category_id', $id);
            $model->save();
        }

        return $productPimCategoryModel->load('filters');
    }
}
