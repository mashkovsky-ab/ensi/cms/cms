<?php

namespace App\Domain\Contents\Actions\ProductCategories;

use App\Domain\Contents\Models\ProductCategory\ProductPimCategory;
use App\Domain\Contents\Models\ProductCategory\ProductPimCategoryFilter;

class DeleteProductPimCategoryAction
{
    public function execute(int $id): void
    {
        ProductPimCategory::destroy($id);
    }
}
