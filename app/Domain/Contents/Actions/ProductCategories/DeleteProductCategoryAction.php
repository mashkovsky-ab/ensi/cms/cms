<?php

namespace App\Domain\Contents\Actions\ProductCategories;

use App\Domain\Contents\Models\ProductCategory\ProductCategory;

class DeleteProductCategoryAction
{
    public function execute(int $id): void
    {
        /** @var ProductCategory $productCategory */
        $productCategory = ProductCategory::query()->with(['pimCategories', 'descendantsWithRelations'])->findOrFail($id);
        $toDelete = [...$productCategory->descendantsWithRelations, $productCategory];

        /** @var ProductCategory $category */
        foreach ($toDelete as $category) {
            foreach ($category->pimCategoriesWithRelations as $pimCategory) {
                $pimCategory->delete();
            }
        }

        $productCategory->delete();
    }
}
