<?php

namespace App\Domain\Contents\Actions\ProductCategories;

use App\Domain\Contents\Models\ProductCategory\ProductPimCategory;
use App\Domain\Contents\Models\ProductCategory\ProductPimCategoryFilter;

class CreateProductPimCategoryAction
{
    public function execute(array $fields, array $filters): ProductPimCategory
    {
        $productPimCategoryModel = new ProductPimCategory();
        $productPimCategoryModel->fill($fields);
        $productPimCategoryModel->save();

        // Create filters
        foreach ($filters as $filter) {
            $productPimCategoryFilterModel = new ProductPimCategoryFilter();
            $productPimCategoryFilterModel->fill($filter);
            $productPimCategoryFilterModel->setAttribute('product_pim_category_id', $productPimCategoryModel->id);
            $productPimCategoryFilterModel->save();
        }

        return $productPimCategoryModel->load(['filters']);
    }
}
