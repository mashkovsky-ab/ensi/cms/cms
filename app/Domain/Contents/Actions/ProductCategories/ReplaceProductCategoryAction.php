<?php

namespace App\Domain\Contents\Actions\ProductCategories;

use App\Domain\Contents\Models\ProductCategory\ProductCategory;

class ReplaceProductCategoryAction
{
    public function execute(int $id, array $fields, array $pimCategories, CreateProductPimCategoryAction $productPimCategoryAction): ProductCategory
    {
        /** @var ProductCategory $productCategoryModel */
        $productCategoryModel = ProductCategory::query()
            ->where('id', $id)
            ->with('pimCategories.filters')
            ->firstOrFail();

        $productCategoryModel->fill($fields);
        $productCategoryModel->save();

        $productCategoryModel->pimCategories->each(fn($pimCategory) => $pimCategory->delete());

        foreach ($pimCategories as $pimCategory) {
            $pimCategory['product_category_id'] = $productCategoryModel->id;
            $productPimCategoryAction->execute($pimCategory, $pimCategory['filters'] ?? []);
        }

        return $productCategoryModel->load(['pimCategories.filters']);
    }
}
