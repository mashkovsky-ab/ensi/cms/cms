<?php

namespace App\Domain\Contents\Actions\ProductCategories;

use App\Http\ApiV1\Support\Concerns\HandlesMassOperation;

class MassDeleteProductCategoryAction
{
    use HandlesMassOperation;

    public function __construct(private DeleteProductCategoryAction $deleteAction)
    {
    }

    public function execute(array $productCategoryIds): void
    {

        $this->each($productCategoryIds, function (int $productCategoryId) {
            $this->deleteAction->execute($productCategoryId);
        });
    }
}
