<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\Banner;
use App\Domain\Contents\Models\BannerButton;

class ReplaceBannersAction
{
    public function execute(int $id, array $fields, ?array $button): Banner
    {
        /** @var Banner $bannerModel */
        $bannerModel = Banner::query()->where('id', $id)->with(['button'])->firstOrFail();

        // Update button
        $buttonId = null;
        if (!is_null($button)) {
            $buttonModel = $bannerModel->button ?? new BannerButton();
            $buttonModel->fill($button);
            $buttonModel->save();
            $buttonId = $buttonModel->id;
        }

        $bannerModel->fill($fields);
        $bannerModel->button_id = $buttonId;
        $bannerModel->save();

        return $bannerModel->refresh();
    }
}
