<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\Menu;
use App\Domain\Contents\Models\MenuItem;
use Kalnoy\Nestedset\Collection as NestedCollection;

class ReplaceMenuTreesAction
{
    public function execute(int $id, array $fields): NestedCollection
    {
        MenuItem::query()->where('menu_id', '=', $id)->delete();
        $this->createMenuItems($id, $fields['items']);

        return Menu::query()->findOrFail($id)->getItemsTree();
    }

    /**
     * @param int $menuId - id меню
     * @param array $items - пункты меню
     * @param int $parentItemId - id родительской категории
     */
    protected function createMenuItems(int $menuId, array $items, int $parentItemId = 0)
    {
        foreach ($items as $item) {
            $modelMenuItem = new MenuItem();
            $modelMenuItem->menu_id = $menuId;
            $modelMenuItem->name = $item['name'];
            $modelMenuItem->url = $item['url'];
            $modelMenuItem->active = $item['active'];

            if (!empty($parentItemId)) {
                $modelMenuItem->parent_id = $parentItemId;
            }
            $modelMenuItem->save();

            if (!empty($item['children'])) {
                $this->createMenuItems($menuId, $item['children'], $modelMenuItem->id);
            }
        }
    }
}
