<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\ProductGroup;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Storage;

class UploadProductGroupFileAction
{
    public function __construct(protected EnsiFilesystemManager $fileManager)
    {
    }

    public function execute(int $id, UploadedFile $file): EnsiFile
    {
        /** @var ProductGroup $productGroup */
        $productGroup = ProductGroup::query()->findOrFail($id);

        $hash = Str::random(20);
        $extension = $file->extension();
        $fileName = "{$productGroup->id}_{$hash}.{$extension}";
        $hashedSubDirs = $this->fileManager->getHashedDirsForFileName($fileName);

        $disk = Storage::disk($this->fileManager->publicDiskName());

        $path = $disk->putFileAs("product-groups/{$hashedSubDirs}", $file, $fileName);
        if (!$path) {
            throw new \RuntimeException("Unable to save file $fileName to directory product-groups/{$hashedSubDirs}");
        }

        if ($productGroup->preview_photo) {
            $disk->delete($productGroup);
        }

        $productGroup->preview_photo = $path;
        $productGroup->save();

        return EnsiFile::public($path);
    }
}
