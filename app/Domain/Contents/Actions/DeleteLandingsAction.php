<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\Landing;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DeleteLandingsAction
{
    public function execute(int $id): void
    {
        Landing::destroy($id);
    }
}
