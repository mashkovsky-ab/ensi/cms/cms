<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\ProductGroup;
use App\Domain\Contents\Models\ProductGroupFilter;
use App\Domain\Contents\Models\ProductGroupProduct;

class ReplaceProductGroupsAction
{
    public function execute(int $id, array $fields, array $filters, array $products): ProductGroup
    {
        // Update ProductGroup
        /** @var ProductGroup $productGroupModel */
        $productGroupModel = ProductGroup::query()->where('id', $id)->with(['filters', 'products'])->firstOrFail();
        $productGroupModel->fill($fields);
        $productGroupModel->save();

        // Update filters
        ProductGroupFilter::destroy($productGroupModel->filters);
        foreach ($filters as $filter) {
            $model = new ProductGroupFilter();
            $model->fill($filter);
            $model->setAttribute('product_group_id', $id);
            $model->save();
        }

        // Update products
        ProductGroupProduct::destroy($productGroupModel->products);
        foreach ($products as $product) {
            $model = new ProductGroupProduct();
            $model->fill($product);
            $model->setAttribute('product_group_id', $id);
            $model->save();
        }

        return $productGroupModel;
    }
}
