<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\Banner;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Storage;

class UploadBannerFileAction
{
    public function __construct(protected EnsiFilesystemManager $fileManager)
    {
    }

    public function execute(int $id, string $type, UploadedFile $file): EnsiFile
    {
        /** @var Banner $banner */
        $banner = Banner::query()->findOrFail($id);

        $column = "{$type}_image";

        $hash = Str::random(20);
        $extension = $file->extension();
        $fileName = "{$banner->id}_{$type}_{$hash}.{$extension}";
        $hashedSubDirs = $this->fileManager->getHashedDirsForFileName($fileName);

        $disk = Storage::disk($this->fileManager->publicDiskName());

        $path = $disk->putFileAs("banners/{$hashedSubDirs}", $file, $fileName);
        if (!$path) {
            throw new \RuntimeException("Unable to save file $fileName to directory banners/{$hashedSubDirs}");
        }

        if ($banner[$column]) {
            $disk->delete($banner[$column]);
        }

        $banner[$column] = $path;
        $banner->save();

        return EnsiFile::public($path);
    }
}
