<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\Landing;

class ReplaceLandingsAction
{
    public function execute(int $id, array $fields): Landing
    {
        /** @var Landing $model */
        $model = Landing::query()->where('id', $id)->firstOrFail();
        $model->fill($fields);
        $model->save();

        return $model;
    }
}
