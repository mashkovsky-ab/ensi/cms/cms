<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\LandingWidgets\LandingWidget;
use Illuminate\Support\Collection;

class ListLandingWidgetsAction
{
    /** @var string[]|array */
    public static $availableWidgets = [
        \App\Domain\Contents\LandingWidgets\LandingTextNoImg::class,
        \App\Domain\Contents\LandingWidgets\LandingSlider::class,
    ];

    public function execute(): Collection
    {
        // Получим для всего набора виджетом их представление WidgetData
        return collect(static::$availableWidgets)
            ->map(function ($widgetClassName) {
                return new $widgetClassName();
            });
    }
}
