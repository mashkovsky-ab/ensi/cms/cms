<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\Banner;
use App\Domain\Contents\Models\BannerButton;

class CreateBannersAction
{
    public function execute(array $fields, ?array $button): Banner
    {
        // Create button
        $buttonId = null;
        if (!is_null($button)) {
            $bannerButtonModel = new BannerButton();
            $bannerButtonModel->fill($button);
            $bannerButtonModel->save();
            $buttonId = $bannerButtonModel->id;
        }

        // Create banner
        $bannerModel = new Banner();
        $bannerModel->fill($fields);
        $bannerModel->button_id = $buttonId;
        $bannerModel->save();

        return $bannerModel->load(['button']);
    }
}
