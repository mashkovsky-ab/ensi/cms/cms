<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\Landing;

class CreateLandingsAction
{
    public function execute(array $fields): Landing
    {
        $model = new Landing();
        $model->fill($fields);
        $model->save();

        return $model;
    }
}
