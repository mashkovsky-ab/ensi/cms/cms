<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\Banner;

class DeleteBannersAction
{
    public function execute(int $id): void
    {
        Banner::destroy($id);
    }
}
