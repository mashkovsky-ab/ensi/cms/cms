<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\ProductGroup;

class DeleteProductGroupsAction
{
    public function execute(int $id): void
    {
        ProductGroup::destroy($id);
    }
}
