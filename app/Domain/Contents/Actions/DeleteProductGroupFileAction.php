<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\ProductGroup;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Storage;

class DeleteProductGroupFileAction
{
    public function __construct(protected EnsiFilesystemManager $fileManager)
    {
    }

    public function execute(int $id): void
    {
        /** @var ProductGroup $productGroup */
        $productGroup = ProductGroup::query()->findOrFail($id);

        $disk = Storage::disk($this->fileManager->publicDiskName());

        if ($productGroup->preview_photo) {
            $disk->delete($productGroup->preview_photo);
        }

        $productGroup->preview_photo = null;
        $productGroup->save();
    }
}
