<?php

namespace App\Domain\Contents\Actions;

use App\Domain\Contents\Models\ProductGroup;
use App\Domain\Contents\Models\ProductGroupFilter;
use App\Domain\Contents\Models\ProductGroupProduct;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CreateProductGroupsAction
{
    public function execute(array $fields, array $filters, array $products): ProductGroup
    {
        // Create ProductGroup
        $productGroupModel = new ProductGroup();
        $productGroupModel->fill($fields);
        $productGroupModel->save();

        // Create filters
        foreach ($filters as $filter) {
            $productGroupFilterModel = new ProductGroupFilter();
            $productGroupFilterModel->fill($filter);
            $productGroupFilterModel->setAttribute('product_group_id', $productGroupModel->id);
            $productGroupFilterModel->save();
        }

        // Create products
        foreach ($products as $product) {
            $productGroupProductModel = new ProductGroupProduct();
            $productGroupProductModel->fill($product);
            $productGroupProductModel->setAttribute('product_group_id', $productGroupModel->id);
            $productGroupProductModel->save();
        }

        return $productGroupModel->load(['filters', 'products']);
    }
}
