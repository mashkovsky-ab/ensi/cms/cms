<?php

namespace App\Domain\Contents\Observers;

use App\Domain\Contents\Models\ProductGroup;
use App\Domain\Contents\Models\ProductGroupFilter;
use App\Domain\Contents\Models\ProductGroupProduct;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Storage;

class ProductGroupObserver
{
    public function __construct(protected EnsiFilesystemManager $filesystemManager)
    {
    }

    public function deleting(ProductGroup $productGroup)
    {
        // Удалим фильтры
        ProductGroupFilter::destroy($productGroup->filters);

        // Удалим связи с продуктами
        ProductGroupProduct::destroy($productGroup->products);

        $this->deleteFile($productGroup->preview_photo);
    }

    protected function deleteFile(?string $path)
    {
        $disk = Storage::disk($this->filesystemManager->publicDiskName());
        if ($path) {
            $disk->delete($path);
        }
    }
}
