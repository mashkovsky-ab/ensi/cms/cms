<?php

namespace App\Domain\Contents\Observers;

use App\Domain\Contents\Models\Banner;
use App\Domain\Contents\Models\BannerButton;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Storage;

class BannerObserver
{
    public function __construct(protected EnsiFilesystemManager $filesystemManager)
    {
    }

    /**
     * Handle the banner "updated" event.
     *
     * @param Banner $banner
     * @return void
     */
    public function updated(Banner $banner)
    {
        // Удалим кнопку
        $oldButtonId = $banner->getOriginal('button_id');
        if (!is_null($oldButtonId) && is_null($banner->button_id)) {
            BannerButton::destroy($oldButtonId);
        }
    }

    /**
     * Handle the banner "deleted" event.
     *
     * @param Banner $banner
     * @return void
     */
    public function deleted(Banner $banner)
    {
        // Удалим кнопку
        $oldButtonId = $banner->getOriginal('button_id');
        if (!is_null($oldButtonId)) {
            BannerButton::destroy($oldButtonId);
        }

        $this->deleteFile($banner->desktop_image);
        $this->deleteFile($banner->tablet_image);
        $this->deleteFile($banner->mobile_image);
    }

    protected function deleteFile(?string $path)
    {
        $disk = Storage::disk($this->filesystemManager->publicDiskName());
        if ($path) {
            $disk->delete($path);
        }
    }
}
