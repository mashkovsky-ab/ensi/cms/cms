<?php

namespace App\Domain\Contents\Observers;

use App\Domain\Contents\Models\ProductGroupProduct;

class ProductGroupProductObserver
{
    /**
     * @param ProductGroupProduct $productGroupLink
     * @return void
     */
    public function saved(ProductGroupProduct $productGroupLink): void
    {
        $this->markForIndexProducts((array)$productGroupLink->product_id);
    }

    /**
     * @param ProductGroupProduct $productGroupLink
     * @return void
     */
    public function deleted(ProductGroupProduct $productGroupLink): void
    {
        $this->markForIndexProducts((array)$productGroupLink->product_id);
    }

    /**
     * @param array|int[] $ids
     */
    private function markForIndexProducts(array $ids): void
    {
        // TODO Добавить отправку сообщений сервису индексирования, когда он появится
    }
}
