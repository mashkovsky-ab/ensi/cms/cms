<?php

namespace App\Domain\Contents\Observers;

use App\Domain\Contents\Models\ProductCategory\ProductPimCategory;
use App\Domain\Contents\Models\ProductCategory\ProductPimCategoryFilter;

class ProductPimCategoryObserver
{
    public function deleting(ProductPimCategory $productPimCategory)
    {
        // Удалим фильтры
        ProductPimCategoryFilter::destroy($productPimCategory->filters);
    }
}
