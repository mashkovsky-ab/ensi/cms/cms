<?php

namespace App\Domain\Contents\Models;

class BannerButtonType
{
    const TYPE_OUTLINE_BLACK = 'outline_black';
    const TYPE_OUTLINE_WHITE = 'outline_white';
    const TYPE_BLACK = 'black';
    const TYPE_WHITE = 'white';

    /** @var string */
    protected $code;

    /** @var string */
    protected $name;

    /**
     * @param string $code
     * @param string $name
     */
    public function __construct($code, $name = null)
    {
        $this->code = $code;

        if (is_null($name)) {
            // Если имя не задали, то найдём его сами
            foreach (static::all() as $type) {
                if ($type->getCode() == $code) {
                    $this->name = $type->getName();
                    break;
                }
            }
        } else {
            $this->name = $name;
        }
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return array|self[]
     */
    public static function all(): array
    {
        return [
            new self(self::TYPE_OUTLINE_BLACK, 'С чёрной обводкой'),
            new self(self::TYPE_OUTLINE_WHITE, 'С белой обводкой'),
            new self(self::TYPE_BLACK, 'Чёрная'),
            new self(self::TYPE_WHITE, 'Белая'),
        ];
    }
}
