<?php

namespace App\Domain\Contents\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $name
 * @property string $code
 * @property bool $active
 * @property bool $is_shown
 * @property string $preview_photo
 * @property int $type_id
 * @property int $banner_id
 * @property string $category_code
 * @property-read Collection|ProductGroupFilter[] $filters
 * @property-read Collection|ProductGroupProduct[] $products
 * @property-read Collection|ProductGroupType[] $type
 * @property-read Collection|Banner[] $banner
 */
class ProductGroup extends Model
{
    const FILLABLE = [
        'name',
        'code',
        'active',
        'is_shown',
        'type_id',
        'banner_id',
        'category_code',
    ];

    protected $fillable = self::FILLABLE;

    public function filters()
    {
        return $this->hasMany(ProductGroupFilter::class);
    }

    public function products()
    {
        return $this->hasMany(ProductGroupProduct::class, 'product_group_id');
    }

    public function type()
    {
        return $this->belongsTo(ProductGroupType::class, 'type_id', 'id');
    }

    public function banner()
    {
        return $this->belongsTo(Banner::class, 'banner_id', 'id');
    }
}
