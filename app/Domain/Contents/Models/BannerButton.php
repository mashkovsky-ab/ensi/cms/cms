<?php

namespace App\Domain\Contents\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $url
 * @property string $text
 * @property string $location
 * @property string $type
 * @property-read BannerButtonLocation $locationModel
 * @property-read BannerButtonType $typeModel
 */
class BannerButton extends Model
{
    const FILLABLE = [
        'url',
        'text',
        'location',
        'type',
    ];

    protected $fillable = self::FILLABLE;

    /**
     * Акксессор location_model к location
     * @param string $value
     * @return BannerButtonLocation
     */
    public function getLocationModelAttribute($value)
    {
        return new BannerButtonLocation($this['location']);
    }

    /**
     * Акксессор type_model к type
     * @param string $value
     * @return BannerButtonType
     */
    public function getTypeModelAttribute($value)
    {
        return new BannerButtonType($this['type']);
    }
}
