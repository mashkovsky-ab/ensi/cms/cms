<?php


namespace App\Domain\Contents\Models;

class File
{
    protected string $path;
    protected string $name;
    protected string $url;

    public function __construct($path, $name, $url = '')
    {
        $this->path = $path;
        $this->name = $name;
        $this->url = $url;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function toArray(): array
    {
        return empty($this->url) ? $this->toShortArray() : $this->toFullArray();
    }

    private function toFullArray(): array
    {
        return [
            'path' => $this->path,
            'name' => $this->name,
            'url' => $this->url,
        ];
    }

    private function toShortArray(): array
    {
        return [
            'path' => $this->path,
            'name' => $this->name,
        ];
    }
}
