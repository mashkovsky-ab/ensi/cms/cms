<?php

namespace App\Domain\Contents\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $code
 */
class ProductGroupType extends Model
{
    const CODE_PROMO = 'promo';
    const CODE_SETS = 'sets';
    const CODE_BRANDS = 'brands';
}
