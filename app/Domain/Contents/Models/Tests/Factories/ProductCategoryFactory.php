<?php

namespace App\Domain\Contents\Models\Tests\Factories;

use App\Domain\Contents\Models\ProductCategory\ProductCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductCategoryFactory extends Factory
{
    protected $model = ProductCategory::class;

    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'url' => $this->faker->url(),
            'active' => $this->faker->boolean(),
            'order' => $this->faker->randomNumber(3),
            'parent_id' => null,
        ];
    }
}
