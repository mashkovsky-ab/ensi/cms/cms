<?php

namespace App\Domain\Contents\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property int $product_group_id
 * @property int $product_id
 * @property int $sort
 * @property-read ProductGroup $group
 */
class ProductGroupProduct extends Model
{
    const FILLABLE = [
        'product_id',
        'sort',
    ];

    protected $fillable = self::FILLABLE;

    public function group(): BelongsTo
    {
        return $this->belongsTo(ProductGroup::class, 'product_group_id');
    }
}
