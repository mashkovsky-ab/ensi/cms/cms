<?php

namespace App\Domain\Contents\Models;

class BannerButtonLocation
{
    const LOCATION_TOP_LEFT = 'top_left';
    const LOCATION_TOP = 'top';
    const LOCATION_TOP_RIGHT = 'top_right';
    const LOCATION_RIGHT = 'right';
    const LOCATION_BOTTOM_RIGHT = 'bottom_right';
    const LOCATION_BOTTOM = 'bottom';
    const LOCATION_BOTTOM_LEFT = 'bottom_left';
    const LOCATION_LEFT = 'left';

    /** @var string */
    protected $code;

    /** @var string|null */
    protected $name;

    /**
     * @param string $code
     * @param string|null $name
     */
    public function __construct($code, $name = null)
    {
        $this->code = $code;

        if (is_null($name)) {
            // Если имя не задали, то найдём его сами
            foreach (static::all() as $location) {
                if ($location->getCode() == $code) {
                    $this->name = $location->getName();
                    break;
                }
            }
        } else {
            $this->name = $name;
        }
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return array|self[]
     */
    public static function all(): array
    {
        return [
            new self(self::LOCATION_TOP_LEFT, 'Сверху слева'),
            new self(self::LOCATION_TOP, 'Сверху'),
            new self(self::LOCATION_TOP_RIGHT, 'Сверху справа'),
            new self(self::LOCATION_RIGHT, 'Справа'),
            new self(self::LOCATION_BOTTOM_RIGHT, 'Снизу справа'),
            new self(self::LOCATION_BOTTOM, 'Снизу'),
            new self(self::LOCATION_BOTTOM_LEFT, 'Снизу слева'),
            new self(self::LOCATION_LEFT, 'Слева'),
        ];
    }
}
