<?php

namespace App\Domain\Contents\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $code
 * @property string $value
 * @property int $product_group_id
 */
class ProductGroupFilter extends Model
{
    const FILLABLE = [
        'code',
        'value',
    ];

    protected $fillable = self::FILLABLE;
}
