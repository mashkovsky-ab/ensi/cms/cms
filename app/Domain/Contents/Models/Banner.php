<?php

namespace App\Domain\Contents\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property bool $active
 * @property string $desktop_image
 * @property string $tablet_image
 * @property string $mobile_image
 * @property int $type_id
 * @property int $button_id
 * @property string $url
 * @property-read BannerType $type
 * @property-read BannerButton $button
 */
class Banner extends Model
{
    const FILLABLE = [
        'name',
        'active',
        'url',
        'type_id',
    ];

    const DESKTOP_IMAGE_TYPE = 'desktop';
    const TABLET_IMAGE_TYPE = 'tablet';
    const MOBILE_IMAGE_TYPE = 'mobile';

    protected $fillable = self::FILLABLE;

    public function type()
    {
        return $this->belongsTo(BannerType::class, 'type_id', 'id');
    }

    public function button()
    {
        return $this->belongsTo(BannerButton::class, 'button_id', 'id');
    }

    /**
     * Получить все возможные типы изображений
     * @return array|string[]
     */
    public static function getAllImageTypes(): array
    {
        return [
            static::DESKTOP_IMAGE_TYPE,
            static::TABLET_IMAGE_TYPE,
            static::MOBILE_IMAGE_TYPE,
        ];
    }
}
