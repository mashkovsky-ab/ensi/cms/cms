<?php

namespace App\Domain\Contents\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property bool $active
 * @property string $name
 * @property string $code
 * @property array $widgets
 */
class Landing extends Model
{
    const INDEX_CODE = 'index';

    const FILLABLE = [
        'id',
        'active',
        'name',
        'code',
        'widgets',
    ];

    protected $fillable = self::FILLABLE;

    protected $casts = [
        'widgets' => 'json',
    ];
}
