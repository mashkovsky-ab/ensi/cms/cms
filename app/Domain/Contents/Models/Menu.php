<?php

namespace App\Domain\Contents\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Kalnoy\Nestedset\Collection as NestedCollection;

/**
 * @property int $id
 * @property string $name
 * @property string $code
 * @property-read Collection|MenuItem[] $items
 */
class Menu extends Model
{
    const HEADER_MAIN_MENU_CODE = 'header_main';
    const HEADER_HELP_MENU_CODE = 'header_help';
    const FOOTER_MAIN_MENU_CODE = 'footer_main';

    public function items()
    {
        return $this->hasMany(MenuItem::class);
    }

    /**
     * items релейшн, но в виде дерева
     * @return NestedCollection
     */
    public function getItemsTree(): NestedCollection
    {
        return $this->items->toTree();
    }
}
