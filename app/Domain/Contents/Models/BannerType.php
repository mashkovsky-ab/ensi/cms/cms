<?php

namespace App\Domain\Contents\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $code
 * @property bool $active
 */
class BannerType extends Model
{
    /** @deprecated */
    const WIDGET_CODE = 'widget';
    /** @deprecated */
    const CATALOG_TOP_CODE = 'catalog_top';
    /** @deprecated */
    const CATALOG_ITEM_CODE = 'catalog';
    /** @deprecated */
    const PRODUCT_GROUP_CODE = 'product_group';
    /** @deprecated */
    const HEADER_CODE = 'header';

    /**
     * Главный баннер
     */
    const MAIN_CODE = 'main';
}
