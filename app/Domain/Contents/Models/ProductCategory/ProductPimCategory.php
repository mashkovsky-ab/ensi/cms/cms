<?php

namespace App\Domain\Contents\Models\ProductCategory;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $code
 * @property int $product_category_id
 * @property-read Collection|ProductPimCategoryFilter[] $filters
 */
class ProductPimCategory extends Model
{
    const FILLABLE = [
        'code',
        'product_category_id',
    ];

    protected $fillable = self::FILLABLE;
    protected $table = 'product_pim_categories';

    /**
     * @return HasMany
     */
    public function filters(): HasMany
    {
        return $this->hasMany(ProductPimCategoryFilter::class);
    }
}
