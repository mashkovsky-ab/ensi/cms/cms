<?php

namespace App\Domain\Contents\Models\ProductCategory;

use App\Domain\Contents\Models\Tests\Factories\ProductCategoryFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Kalnoy\Nestedset\DescendantsRelation;
use Kalnoy\Nestedset\NodeTrait;

/**
 * @property int $id
 * @property string $name
 * @property string $url
 * @property bool $active
 * @property int $parent_id
 * @property int order
 * @property-read Collection|ProductPimCategory[] $pimCategories категории Pim
 * @property-read Collection|ProductPimCategory[] $pimCategoriesWithRelations категории Pim со связями
 * @property-read Collection|ProductCategory[] $categories дочерние категории
 * @property-read Collection|ProductCategory[] $children дочерние категории
 * @property-read Collection|ProductCategory[] $descendantsWithRelations все дочерние категории со связями
 */
class ProductCategory extends Model
{
    use NodeTrait;

    const FILLABLE = [
        'name',
        'url',
        'active',
        'parent_id',
        'order',
    ];

    protected $fillable = self::FILLABLE;
    protected $table = 'product_categories';

    protected static function boot()
    {
        parent::boot();

        self::deleted(function (self $category) {
            ProductCategory::query()->fixTree();
        });
    }

    /**
     * @return BelongsTo
     */
    public function parentCategory(): BelongsTo
    {
        return $this->parent();
    }

    /**
     * @return HasMany
     */
    public function categories(): HasMany
    {
        return $this->children();
    }

    /**
     * @return DescendantsRelation
     */
    public function descendantsWithRelations(): DescendantsRelation
    {
        return $this->descendants()->with('pimCategoriesWithRelations');
    }

    /**
     * @return HasMany
     */
    public function pimCategories(): HasMany
    {
        return $this->hasMany(ProductPimCategory::class, 'product_category_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function pimCategoriesWithRelations(): HasMany
    {
        return $this->hasMany(ProductPimCategory::class, 'product_category_id', 'id')->with('filters');
    }

    public static function factory(): ProductCategoryFactory
    {
        return ProductCategoryFactory::new();
    }
}
