<?php

namespace App\Domain\Contents\Models\ProductCategory;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $code
 * @property string $value
 * @property int $product_pim_category_id

 */
class ProductPimCategoryFilter extends Model
{
    const FILLABLE = [
        'code',
        'value',
    ];

    protected $fillable = self::FILLABLE;
    protected $table = 'product_pim_category_filters';
}
