<?php

namespace App\Domain\Contents\LandingWidgets;

use App\Domain\Contents\LandingWidgets\Props\SimpleProps;
use App\Domain\Contents\Models\File;

class LandingBanner extends LandingWidget
{
    /**
     * Code of widget.
     *
     * @var string
     */
    protected $code = "LandingSlider";

    /**
     * Name of widget.
     *
     * @var string
     */
    protected $name = "Слайдер";

    /**
     * Component of widget.
     *
     * @var string
     */
    protected $component = "LandingSlider";

    /**
     * Link to widget help page.
     *
     * @var string
     */
    protected $helpLink = "/help#landing-slider-help";

    public function __construct($customPropsValues = [])
    {
        parent::__construct($customPropsValues);

        $this->previewBig = new File('198x85?text=No+image', '198x85', 'http://placehold.it/600x200?text=No+image');
        $this->previewSmall = new File('198x85?text=No+image', '198x85', 'http://placehold.it/198x85?text=No+image');
    }

    /**
     * All component props.
     *
     * @return array
     */
    public function getComponentProps()
    {
        return [
            "id" => (new SimpleProps("Баннер"))
                ->banner()
                ->required()
                ->toArray(),
        ];
    }
}
