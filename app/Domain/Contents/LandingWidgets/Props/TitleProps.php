<?php

namespace App\Domain\Contents\LandingWidgets\Props;

/**
 * Class TitleProps
 * @package App\Domain\Contents\LandingWidgets\Props
 */
class TitleProps extends ComplexProps
{
    /**
     * TitleProps constructor.
     * @param string $label
     * @param bool $required
     * @param string $type
     * @param string $default
     */
    public function __construct(string $label = "Заголовок", bool $required = false, string $type = 'complex', string $default = '')
    {
        parent::__construct($label, $required, $type, $default);

        $this->props = [
            "value" => (new SimpleProps($label))->required()->toArray(),
            "style" => (new StyleProps())->toArray(),
        ];
    }
}
