<?php

namespace App\Domain\Contents\LandingWidgets\Props;

/**
 * Class MarginProps
 * @package App\Domain\Contents\LandingWidgets\Props
 */
class MarginProps extends SimpleProps
{
    /**
     * MarginProps constructor.
     * @param string $marginType
     */
    public function __construct($marginType = "top")
    {
        $label = ($marginType == "top") ? "Верхний отступ" : "Нижний отступ";
        parent::__construct($label);

        $this
            ->addExtraField('is_margin', true)
            ->setTooltip("Введите число, соответствующее отступу в пикселах");
    }
}
