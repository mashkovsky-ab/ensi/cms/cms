<?php

namespace App\Domain\Contents\LandingWidgets\Props;

/**
 * Class MarginBottomProps
 * @package App\Domain\Contents\LandingWidgets\Props
 */
class MarginBottomProps extends MarginProps
{
    /**
     * MarginBottomProps constructor.
     */
    public function __construct()
    {
        parent::__construct("bottom");
    }
}
