<?php

namespace App\Domain\Contents\LandingWidgets\Props;

/**
 * Class ProjectsPictureProps
 * @package App\Domain\Contents\LandingWidgets\Props
 */
class ProjectsPictureProps extends PictureProps
{
    protected function reloadProps()
    {
        parent::reloadProps();

        unset($this->props["lazy"]);
        $this->props["href"] = (new SimpleProps("Ссылка"))
            ->textarea()
            ->setTooltip(SimpleProps::LINK_TOOLTIP)
            ->toArray();
    }
}
