<?php

namespace App\Domain\Contents\LandingWidgets\Props;

/**
 * Class MarginTopProps
 * @package App\Domain\Contents\LandingWidgets\Props
 */
class MarginTopProps extends MarginProps
{
    /**
     * MarginTopProps constructor.
     */
    public function __construct()
    {
        parent::__construct("top");
    }
}
