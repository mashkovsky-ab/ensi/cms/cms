<?php

namespace App\Domain\Contents\LandingWidgets\Props;

/**
 * Class BaseProps
 * @package App\Domain\Contents\LandingWidgets\Props
 */
abstract class BaseProps
{
    /**
     * @var string
     */
    protected $label;

    /**
     * @var bool
     */
    protected $required;

    /**
     * @var string
     */
    protected $default;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var bool
     */
    protected $hidden = false;

    /**
     * @var bool
     */
    protected $multiple = false;

    /**
     * @var array
     */
    protected $conditions = [];

    /**
     * @var array
     */
    protected $extraFields = [];

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function addExtraField($key, $value)
    {
        $this->extraFields[$key] = $value;

        return $this;
    }

    /**
     * @param $label
     * @return $this
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @param $required
     * @return $this
     */
    public function setRequired($required)
    {
        $this->required = $required;

        return $this;
    }

    /**
     * @return $this
     */
    public function required()
    {
        $this->required = true;

        return $this;
    }

    /**
     * Set type textarea
     * @return $this
     */
    public function string($maxLength = 1000)
    {
        $this->type = 'string';
        $this->extraFields['max_length'] = $maxLength;

        return $this;
    }

    /**
     * Set type textarea
     * @return $this
     */
    public function textarea($size = 2)
    {
        $this->type = 'textarea';
        $this->extraFields['textarea_size'] = $size;

        return $this;
    }

    /**
     * Set type boolean
     * @return $this
     */
    public function boolean()
    {
        $this->type = 'boolean';

        return $this;
    }

    /**
     * Set hidden
     * @return $this
     */
    public function hidden()
    {
        $this->hidden = true;

        return $this;
    }

    /**
     * Set multiple
     * @return $this
     */
    public function multiple($minCount = null, $maxCount = null)
    {
        $this->multiple = true;

        if (!is_null($minCount)) {
            $this->minCount($minCount);
        }

        if (!is_null($maxCount)) {
            $this->maxCount($maxCount);
        }

        return $this;
    }

    /**
     * @param int $minCount
     * @return $this
     */
    public function minCount($minCount)
    {
        $this->addExtraField('min_count', $minCount);

        return $this;
    }

    /**
     * @param int $maxCount
     * @return $this
     */
    public function maxCount($maxCount)
    {
        $this->addExtraField('max_count', $maxCount);

        return $this;
    }

    /**
     * @param int $count
     * @return $this
     */
    public function fixedCount($count)
    {
        $this->minCount($count);
        $this->maxCount($count);

        return $this;
    }

    /**
     * Set type widget
     * @param string $widgetClass
     * @param string $helpLink
     * @return $this
     */
    public function widget($widgetClass, $widgetProps = null, $helpLink = null)
    {
        $this->type = 'widget';
        $this->extraFields['widgetClass'] = $widgetClass;

        if (!is_null($widgetProps)) {
            $this->extraFields['widgetProps'] = $widgetProps;
        }

        if (!is_null($helpLink)) {
            $this->extraFields['widgetHelpLink'] = $helpLink;
        }

        return $this;
    }

    /**
     * Set type select
     * @param $options
     * @return $this
     */
    public function select($options)
    {
        $this->type = 'select';
        $this->extraFields['selectType'] = 'select';
        $this->extraFields['options'] = $options;

        return $this;
    }

    /**
     * Set type radio select
     * @param $options
     * @return $this
     */
    public function radio($options)
    {
        $this->type = 'select';
        $this->extraFields['selectType'] = 'radio';
        $this->extraFields['options'] = $options;

        return $this;
    }

    /**
     * Set type banner
     * @return $this
     */
    public function banner()
    {
        $this->type = 'banner';

        return $this;
    }

    /**
     * @param $default
     * @return $this
     */
    public function setDefault($default)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * @param $field
     * @param $values
     * @return $this
     */
    public function if($field, $values)
    {
        $this->conditions[$field] = $values;

        return $this;
    }

    /**
     * @param $tooltip
     * @return $this
     */
    public function setTooltip($tooltip, $tooltipLink = null)
    {
        $this->extraFields['tooltip'] = $tooltip;
        if ($tooltipLink) {
            $this->extraFields['tooltip_href'] = $tooltipLink;
        }

        return $this;
    }

    /**
     * @return array
     */
    protected function toArray()
    {
        return array_merge(
            [
                "label" => $this->label,
                "required" => $this->required,
                "default" => $this->default,
                "type" => $this->type,
                "hidden" => $this->hidden,
                "isInShownList" => true, // by default
                "conditions" => $this->conditions,
            ],
            $this->extraFields
        );
    }
}
