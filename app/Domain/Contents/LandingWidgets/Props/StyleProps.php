<?php

namespace App\Domain\Contents\LandingWidgets\Props;

/**
 * Class StyleProps
 * @package App\Domain\Contents\LandingWidgets\Props
 */
class StyleProps extends SimpleProps
{
    /**
     * StyleProps constructor.
     * @param string $label
     * @param bool $required
     * @param string $type
     * @param string $default
     */
    public function __construct($label = "Дополнительные стили", $required = false, $type = 'string', $default = '')
    {
        parent::__construct($label, $required, $type, $default);

        $this->textarea()
            ->setSpoiler("Переопределить стили")
            ->setTooltip("Впишите нужные стили CSS. Например, background-color: #858585; color: #FFF;");
    }
}
