<?php

namespace App\Domain\Contents\LandingWidgets\Props;

/**
 * Class PictureProps
 * @package App\Domain\Contents\LandingWidgets\Props
 */
class PictureProps extends ComplexProps
{
    const SRC_TOOLTIP = "Введите URL-ссылку на изображение в формате jpeg или png";

    /**
     * @var bool
     */
    protected $isContentPicture;

    /**
     * @var bool
     */
    protected $tooltipsLink = null;

    /**
     * PictureProps constructor.
     *
     * @param string $label
     * @param bool   $required
     * @param string $type
     * @param string $default
     * @param bool   $isContentPicture
     */
    public function __construct(
        string $label = 'Изображение',
        bool $required = false,
        string $type = 'complex',
        string $default = '',
        bool $isContentPicture = true
    ) {
        parent::__construct($label, $required, $type, $default);

        $this->isContentPicture = $isContentPicture;
        $this->reloadProps();
    }

    /**
     * @param $value
     * @return $this
     */
    public function isContentPicture($value)
    {
        $this->isContentPicture = (bool)$value;
        $this->reloadProps();

        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setTooltipsLink($value)
    {
        $this->tooltipsLink = $value;
        $this->reloadProps();

        return $this;
    }

    protected function reloadProps()
    {
        $this->props = [
            "src" => (new SimpleProps("URL"))
                ->required()
                ->setTooltip(static::SRC_TOOLTIP, $this->tooltipsLink)
                ->toArray(),
            "alt" => (new SimpleProps("Альтернативный текст"))
                ->textarea()
                ->setRequired($this->isContentPicture)
                ->setTooltip(
                    "Введите текст, нужный для скринридеров (a11y), поисковых роботов (SEO). Также он будет выведен в качестве фоллбека, если изображение не сможет загрузиться",
                    $this->tooltipsLink
                )
                ->toArray(),
            "media" => (new ComplexProps("Адаптивные изображения"))
                ->multiple()
                ->setTooltip(
                    "Предоставление альтернативных изображений на разных разрешениях. Гарантирует использование указанного изображения на разрешениях, меньше указанного",
                    $this->tooltipsLink
                )
                ->setProps([
                    "width" => (new SimpleProps('Ширина экрана'))
                        ->required()
                        ->setTooltip(
                            "Укажите в пикселях ширину экрана, при которой должно быть загружено другое изображение. Например, 1024",
                            $this->tooltipsLink
                        )
                        ->toArray(),
                    "srcSet" => (new SimpleProps('URL'))
                        ->required()
                        ->setTooltip(static::SRC_TOOLTIP, $this->tooltipsLink)
                        ->toArray(),
                ])
                ->toArray(),
            "lazy" => (new SimpleProps("Ленивая загрузка"))->boolean()->setDefault(true)->hidden()->toArray(),
            "style" => (new StyleProps())->toArray(),
        ];
    }
}
