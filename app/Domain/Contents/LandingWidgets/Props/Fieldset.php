<?php

namespace App\Domain\Contents\LandingWidgets\Props;

/**
 * Class Fieldset
 * Fieldset is not a real prop, it's just a named group of props.
 *
 * @package App\Domain\Contents\LandingWidgets\Props
 */
class Fieldset extends ComplexProps
{
    /**
     * Fieldset constructor.
     * @param string $label
     * @param bool $required
     * @param string $type
     * @param string $default
     */
    public function __construct(string $label = '', bool $required = false, string $type = 'complex', string $default = '')
    {
        parent::__construct($label, $required, $type, $default);

        $this->extraFields['fieldset_group'] = true;
    }
}
