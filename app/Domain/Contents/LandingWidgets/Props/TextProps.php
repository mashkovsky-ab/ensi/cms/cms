<?php

namespace App\Domain\Contents\LandingWidgets\Props;

/**
 * Class TextProps
 * @package App\Domain\Contents\LandingWidgets\Props
 */
class TextProps extends ComplexProps
{
    /**
     * TitleProps constructor.
     * @param string $label
     * @param bool $required
     * @param string $type
     * @param string $default
     */
    public function __construct(string $label = "Текст", bool $required = false, string $type = 'complex', string $default = '')
    {
        parent::__construct($label, $required, $type, $default);

        $this->props = [
            "value" => (new SimpleProps('Текст'))->textarea()->toArray(),
            "style" => (new StyleProps())->toArray(),
        ];
    }
}
