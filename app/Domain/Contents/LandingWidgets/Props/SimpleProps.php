<?php

namespace App\Domain\Contents\LandingWidgets\Props;

/**
 * Class SimpleProps
 * @package App\Domain\Contents\LandingWidgets\Props
 */
class SimpleProps extends BaseProps
{
    const LINK_TOOLTIP = "Введите URL-ссылку на нужную страницу. При клике по картинке произойдет переход по этой ссылке";
    const ICON_TOOLTIP = "Вставьте URL-ссылку на изображение в формате svg";

    /**
     * @var string
     */
    protected $arrayItemCode = "item";

    /**
     * @var array
     */
    protected $arrayItemLabel = "Элемент";

    /**
     * @var array
     */
    protected $arrayItemAddParams = [];

    /**
     * Fields not to copy into array items.
     *
     * @var array
     */
    protected $arrayItemBlacklist = ["conditions", "tooltip", "tooltip_href"];

    /**
     * SimpleProps constructor.
     * @param $label
     * @param bool $required
     * @param string $type
     * @param string $default
     */
    public function __construct($label = '', $required = false, $type = 'string', $default = '')
    {
        $this->label = $label;
        $this->required = $required;
        $this->type = $type;
        $this->default = $default;
    }

    /**
     * Set multiple
     * @param $code
     * @param $label
     * @return $this
     */
    public function setArrayItemParams($code, $label, $addParams = [])
    {
        $this->arrayItemCode = $code;
        $this->arrayItemLabel = $label;
        $this->arrayItemAddParams = $addParams;

        return $this;
    }

    /**
     * @param $label
     * @return $this
     */
    public function setSpoiler($spoiler)
    {
        $this->addExtraField('spoiler', $spoiler);

        return $this;
    }

    /**
     * @return $this
     */
    public function setIsPrice()
    {
        $this->addExtraField('is_price', true);

        return $this;
    }

    /**
     * @return $this
     */
    public function setIsNumeric()
    {
        $this->addExtraField('is_numeric', true);

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $prop = parent::toArray();

        $arrayItem = $prop;
        $arrayItem['label'] = $this->arrayItemLabel;

        foreach ($this->arrayItemBlacklist as $fieldNotToCopy) {
            unset($arrayItem[$fieldNotToCopy]);
        }

        foreach ($this->arrayItemAddParams as $field => $value) {
            $arrayItem[$field] = $value;
        }

        return $this->multiple
            ? array_merge($prop, ["multiple" => $this->multiple, "array_item" => [$this->arrayItemCode => $arrayItem]])
            : $prop;
    }
}
