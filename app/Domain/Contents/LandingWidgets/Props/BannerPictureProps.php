<?php

namespace App\Domain\Contents\LandingWidgets\Props;

/**
 * Class BannerPictureProps
 * @package App\Domain\Contents\LandingWidgets\Props
 */
class BannerPictureProps extends PictureProps
{
    protected function reloadProps()
    {
        parent::reloadProps();

        $this->props["type"] = (new SimpleProps("Тип"))
            ->required()
            ->radio([
                "last" => "Снизу",
                "first" => "Сверху",
            ])
            ->setDefault("last")
            ->toArray();
    }
}
