<?php

namespace App\Domain\Contents\LandingWidgets;

use App\Domain\Contents\LandingWidgets\Props\SimpleProps;
use App\Domain\Contents\Models\File;

class LandingTextNoImg extends LandingWidget
{
    /**
     * Code of widget.
     *
     * @var string
     */
    protected $code = "LandingTextNoImg";

    /**
     * Name of widget.
     *
     * @var string
     */
    protected $name = "Текстовый блок (без картинки)";

    /**
     * Component of widget.
     *
     * @var string
     */
    protected $component = "LandingTextNoImg";

    public function __construct($customPropsValues = [])
    {
        parent::__construct($customPropsValues);

        $this->previewBig = new File('198x85?text=No+image', '198x85', 'http://placehold.it/600x200?text=No+image');
        $this->previewSmall = new File('198x85?text=No+image', '198x85', 'http://placehold.it/198x85?text=No+image');
    }

    /**
     * Link to widget help page.
     *
     * @var string
     */
    protected $helpLink = "/help#landing-text-no-img-help";

    public function getComponentProps()
    {
        return [
            "title" => (new SimpleProps("Заголовок"))->toArray(),
            "text" => (new SimpleProps("Текст"))->toArray(),
        ];
    }

    /**
     * @inheritDoc
     */
    public function getDefaultPropsValues(): array
    {
        return [
            "title" => static::DEFAULT_TEXT,
            "text" => static::DEFAULT_TEXT,
        ];
    }
}
