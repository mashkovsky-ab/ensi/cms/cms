<?php

namespace App\Domain\Contents\LandingWidgets;

use App\Domain\Contents\Models\File;

abstract class LandingWidget
{
    const DEFAULT_TEXT = "Введите текст";

    const DEFAULT_SELECT_TEXT = "Выберите значение";

    /**
     * Code of widget.
     *
     * @var string
     */
    protected $code;

    /**
     * Name of widget.
     *
     * @var string
     */
    protected $name;

    /**
     * Component of widget.
     *
     * @var string
     */
    protected $component;

    /**
     * Big preview of widget.
     *
     * @var File
     */
    protected $previewBig;

    /**
     * Small preview of widget.
     *
     * @var File
     */
    protected $previewSmall;

    /**
     * Link to widget help page.
     *
     * @var string
     */
    protected $helpLink;

    /**
     * Custom props.
     *
     * @var array
     */
    protected $customProps = [];

    /**
     * Custom default props values.
     *
     * @var array
     */
    protected $customPropsValues = [];

    /**
     * LandingWidget constructor.
     *
     * @param array $customPropsValues
     */
    public function __construct($customPropsValues = [])
    {
        $this->customPropsValues = $customPropsValues ?: [];
    }

    /**
     * @param $helpLink
     */
    public function setHelpLink($helpLink)
    {
        $this->helpLink = $helpLink;
    }

    /**
     * @param array $customProps
     */
    public function setCustomProps($customProps = [])
    {
        $this->customProps = $customProps;
    }

    /**
     * All component props.
     *
     * array [key => App\Domain\Contents\LandingWidgets\Props]
     *
     * @return array
     */
    public function getComponentProps()
    {
        return [];
    }

    /**
     * Default props values for widget.
     *
     * Should consider the getComponentProps key structure,
     * but instead of describing props each key corresponds to a default value.
     *
     * @return array
     */
    public function getDefaultPropsValues(): array
    {
        return [];
    }

    /**
     * Widget props (with default and custom values).
     *
     * @return array
     */
    public function getWidgetProps()
    {
        $props = $this->getComponentProps();
        if ($this->customProps) {
            $props = $this->customProps;
        }

        // flatten fieldset props
        $flattenProps = [];
        foreach ($props as $k => $prop) {
            if (isset($prop["fieldset_group"]) && $prop["fieldset_group"]) {
                foreach ($prop["complex"] as $j => $innerProp) {
                    $innerProp["fieldset"] = [
                        "code" => $k,
                        "label" => $prop["label"],
                        "isInShownList" => true, // by default
                        "conditions" => $prop["conditions"],
                    ];

                    $flattenProps[$j] = $innerProp;
                }
            } else {
                $flattenProps[$k] = $prop;
            }
        }
        $props = $flattenProps;

        $defaultPropsValues = $this->getDefaultPropsValues();

        $props = collect($props)->map(function ($params, $code) use ($defaultPropsValues) {
            if (isset($defaultPropsValues[$code])) {
                $params["default"] = $defaultPropsValues[$code];
            }

            if (isset($this->customPropsValues[$code])) {
                $params["default"] = $this->customPropsValues[$code];
            }

            $params["code"] = $code;

            return $params;
        })->toArray();

        foreach ($props as $k => $prop) {
            $this->fillPropRecursively($props[$k]);
        }

        return $props;
    }

    /**
     * Widget props values (with default and custom values).
     *
     * @return array
     */
    public function getWidgetPropsDefaultValues()
    {
        return collect($this->getWidgetProps())->pluck("default", "code")->toArray();
    }

    /**
     * @return array
     */
    public function getWidgetData()
    {
        return [
            "name" => $this->name,
            "widgetCode" => $this->code,
            "component" => $this->component,
            "previewBig" => $this->previewBig,
            "previewSmall" => $this->previewSmall,
            "props" => collect($this->getWidgetProps())->toArray(),
        ];
    }

    /**
     * @param array $prop
     */
    protected function fillPropRecursively(&$prop)
    {
        if (isset($prop["multiple"]) && $prop["multiple"]) {
            foreach ($prop["array_item"] as $k => $childProp) {
                $prop["array_item"][$k]["code"] = $k;
                $this->fillPropRecursively($prop["array_item"][$k]);
            }

            if (!isset($prop["array"])) {
                $prop["array"] = [];
            }

            if (is_array($prop["default"])) {
                foreach ($prop["default"] as $i => $childComplexDefaultValues) {
                    if (!isset($prop["array"][$i])) {
                        $prop["array"][$i] = $prop["array_item"] ?: [];
                    }
                }
            }

            foreach ($prop["array"] as $i => $childArrayItem) {
                foreach ($prop["array"][$i] as $k => $childArrayItemProp) {
                    $prop["array"][$i][$k]["code"] = $k;
                    if (isset($prop["default"][$i][$k])) {
                        $prop["array"][$i][$k]["default"] = $prop["default"][$i][$k];
                    }

                    $this->fillPropRecursively($prop["array"][$i][$k]);
                }
            }
        } elseif ($prop["type"] == "complex") {
            foreach ($prop["complex"] as $k => $childProp) {
                $prop["complex"][$k]["code"] = $k;
                if (isset($prop["default"][$k])) {
                    $prop["complex"][$k]["default"] = $prop["default"][$k];
                }

                $this->fillPropRecursively($prop["complex"][$k]);
            }
        } elseif ($prop["type"] == "widget") {
            $widgetClass = $prop["widgetClass"];
            $widgetDefaultParams = $prop["default"];
            $widget = new $widgetClass($widgetDefaultParams);
            if ($widget instanceof LandingWidget) {
                if (isset($prop["widgetHelpLink"]) && $prop["widgetHelpLink"]) {
                    $widget->setHelpLink($prop["widgetHelpLink"]);
                }

                if (isset($prop["widgetProps"]) && $prop["widgetProps"]) {
                    $widget->setCustomProps($prop["widgetProps"]);
                }

                $prop["widget"] = $widget->getWidgetData();
            }
        }
    }
}
