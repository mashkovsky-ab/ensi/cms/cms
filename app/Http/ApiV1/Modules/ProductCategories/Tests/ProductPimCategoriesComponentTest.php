<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

//test('POST /api/v1/contents/product-categories/pim-categories 200', function () {
//    postJson('/api/v1/contents/product-categories/pim-categories')
//        ->assertStatus(200);
//});
//
//test('POST /api/v1/contents/product-categories/pim-categories 400', function () {
//    postJson('/api/v1/contents/product-categories/pim-categories')
//        ->assertStatus(400);
//});
//
//test('POST /api/v1/contents/product-categories/pim-categories 404', function () {
//    postJson('/api/v1/contents/product-categories/pim-categories')
//        ->assertStatus(404);
//});
//
//test('GET /api/v1/contents/product-categories/pim-categories/{id} 200', function () {
//    getJson('/api/v1/contents/product-categories/pim-categories/{id}')
//        ->assertStatus(200);
//});
//
//test('GET /api/v1/contents/product-categories/pim-categories/{id} 404', function () {
//    getJson('/api/v1/contents/product-categories/pim-categories/{id}')
//        ->assertStatus(404);
//});
//
//test('PUT /api/v1/contents/product-categories/pim-categories/{id} 200', function () {
//    putJson('/api/v1/contents/product-categories/pim-categories/{id}')
//        ->assertStatus(200);
//});
//
//test('PUT /api/v1/contents/product-categories/pim-categories/{id} 400', function () {
//    putJson('/api/v1/contents/product-categories/pim-categories/{id}')
//        ->assertStatus(400);
//});
//
//test('PUT /api/v1/contents/product-categories/pim-categories/{id} 404', function () {
//    putJson('/api/v1/contents/product-categories/pim-categories/{id}')
//        ->assertStatus(404);
//});
//
//test('DELETE /api/v1/contents/product-categories/pim-categories/{id} 200', function () {
//    deleteJson('/api/v1/contents/product-categories/pim-categories/{id}')
//        ->assertStatus(200);
//});
//
//test('DELETE /api/v1/contents/product-categories/pim-categories/{id} 404', function () {
//    deleteJson('/api/v1/contents/product-categories/pim-categories/{id}')
//        ->assertStatus(404);
//});
//
//test('POST /api/v1/contents/product-categories/pim-categories:search 200', function () {
//    postJson('/api/v1/contents/product-categories/pim-categories:search')
//        ->assertStatus(200);
//});
//
//test('POST /api/v1/contents/product-categories/pim-categories:search 400', function () {
//    postJson('/api/v1/contents/product-categories/pim-categories:search')
//        ->assertStatus(400);
//});
//
//test('POST /api/v1/contents/product-categories/pim-categories:search 404', function () {
//    postJson('/api/v1/contents/product-categories/pim-categories:search')
//        ->assertStatus(404);
//});
