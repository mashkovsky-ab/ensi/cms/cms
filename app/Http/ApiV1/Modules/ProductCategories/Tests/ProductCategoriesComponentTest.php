<?php

use App\Domain\Contents\Models\ProductCategory\ProductCategory;
use App\Domain\Contents\Models\ProductCategory\ProductPimCategory;
use App\Domain\Contents\Models\ProductCategory\ProductPimCategoryFilter;
use App\Http\ApiV1\Modules\ProductCategories\Tests\Factories\ProductCategoryRequestFactory;
use App\Http\ApiV1\Modules\ProductCategories\Tests\Factories\ProductPimCategoryRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/contents/product-categories success', function () {
    $productPimCategoryData = ProductPimCategoryRequestFactory::new()->make();
    $productCategoryData = ProductCategoryRequestFactory::new()
        ->withPimCategory($productPimCategoryData)
        ->make();


    postJson('/api/v1/contents/product-categories', $productCategoryData)
        ->assertStatus(201)
        ->assertJsonPath('data.name', $productCategoryData['name'])
        ->assertJsonPath('data.url', $productCategoryData['url'])
        ->assertJsonPath('data.order', $productCategoryData['order'])
        ->assertJsonPath('data.parent_id', $productCategoryData['parent_id']);

    /** @var ProductCategory $productCategoryCreated */
    $productCategoryCreated = ProductCategory::query()->where('url', $productCategoryData['url'])->first();
    $this->assertNotNull($productCategoryCreated);

    assertDatabaseHas((new ProductCategory())->getTable(), [
        "name" => $productCategoryCreated->name,
        "url" => $productCategoryCreated->url,
        "order" => $productCategoryCreated->order,
        "parent_id" => $productCategoryCreated->parent_id,
    ]);


    /** @var ProductPimCategory $productPimCategoryCreated */
    $productPimCategoryCreated = ProductPimCategory::query()
        ->where('product_category_id', $productCategoryCreated->id)
        ->first();
    $this->assertNotNull($productPimCategoryCreated);

    assertDatabaseHas((new ProductPimCategory())->getTable(), [
        "code" => $productPimCategoryData['code'],
        "product_category_id" => $productCategoryCreated->id,
    ]);


    /** @var ProductPimCategoryFilter $productPimCategoryFilterCreated */
    $productPimCategoryFilterCreated = ProductPimCategoryFilter::query()
        ->where('product_pim_category_id', $productPimCategoryCreated->id)
        ->first();
    $this->assertNotNull($productPimCategoryFilterCreated);

    assertDatabaseHas((new ProductPimCategoryFilter())->getTable(), [
        "code" => $productPimCategoryData['filters'][0]['code'],
        "value" => $productPimCategoryData['filters'][0]['value'],
        "product_pim_category_id" => $productPimCategoryCreated->id,
    ]);

});

test('POST /api/v1/contents/product-categories 400 when parent category does not exist', function () {
    $productCategoryData = ProductCategoryRequestFactory::new()->make(['parent_id' => 1]);
    postJson('/api/v1/contents/product-categories', $productCategoryData)
        ->assertStatus(400);
});


test('GET /api/v1/contents/product-categories/{id} 200', function () {
    $productCategory = ProductCategory::factory()->create();

    getJson("/api/v1/contents/product-categories/{$productCategory->id}",)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $productCategory->id);
});

test('PUT /api/v1/contents/product-categories/{id} 200', function () {
    /** @var ProductCategory $productCategory */
    $productCategory = ProductCategory::factory()->create();

    $productPimCategoryData = ProductPimCategoryRequestFactory::new()->make();
    $productCategoryData = ProductCategoryRequestFactory::new()
        ->withPimCategory($productPimCategoryData)
        ->make();

    putJson('/api/v1/contents/product-categories/' . $productCategory->id, $productCategoryData)
        ->assertStatus(200)
        ->assertJsonPath('data.name', $productCategoryData['name'])
        ->assertJsonPath('data.url', $productCategoryData['url'])
        ->assertJsonPath('data.order', $productCategoryData['order'])
        ->assertJsonPath('data.parent_id', $productCategoryData['parent_id']);

    /** @var ProductCategory $productCategoryCreated */
    $productCategoryUpdated = ProductCategory::query()->where('id', $productCategory->id)->first();
    $this->assertNotNull($productCategoryUpdated);

    assertDatabaseHas((new ProductCategory())->getTable(), [
        "name" => $productCategoryData['name'],
        "url" => $productCategoryData['url'],
        "order" => $productCategoryData['order'],
        "parent_id" => $productCategoryData['parent_id'],
    ]);


    /** @var ProductPimCategory $productPimCategoryCreated */
    $productPimCategoryReCreated = ProductPimCategory::query()
        ->where('product_category_id', $productCategoryUpdated->id)
        ->first();
    $this->assertNotNull($productPimCategoryReCreated);

    assertDatabaseHas((new ProductPimCategory())->getTable(), [
        "code" => $productPimCategoryData['code'],
        "product_category_id" => $productCategoryUpdated->id,
    ]);


    /** @var ProductPimCategoryFilter $productPimCategoryFilterCreated */
    $productPimCategoryFilterCreated = ProductPimCategoryFilter::query()
        ->where('product_pim_category_id', $productPimCategoryReCreated->id)
        ->first();
    $this->assertNotNull($productPimCategoryFilterCreated);

    assertDatabaseHas((new ProductPimCategoryFilter())->getTable(), [
        "code" => $productPimCategoryData['filters'][0]['code'],
        "value" => $productPimCategoryData['filters'][0]['value'],
        "product_pim_category_id" => $productPimCategoryReCreated->id,
    ]);
});

test('PUT /api/v1/contents/product-categories/{id} 400', function () {
    /** @var ProductCategory $productCategory */
    $productCategory = ProductCategory::factory()->create();

    $productPimCategoryData = ProductPimCategoryRequestFactory::new()->make();
    $productCategoryData = ProductCategoryRequestFactory::new()
        ->withPimCategory($productPimCategoryData)
        ->make(['parent_id' => 1]);

    putJson('/api/v1/contents/product-categories/' . $productCategory->id, $productCategoryData)
        ->assertStatus(400);
});


test('DELETE /api/v1/contents/product-categories/{id} 200', function () {
    /** @var ProductCategory $productCategory */
    $productCategory = ProductCategory::factory()->create();
    /** @var ProductCategory $childLvl1 */
    $childLvl1 = ProductCategory::factory()->create(['parent_id' => $productCategory->id]);

    deleteJson('/api/v1/contents/product-categories/' . $productCategory->id)
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    /** @var ProductCategory $productCategory */
    $productCategory = ProductCategory::query()->where('id', $productCategory->id)->first();
    $this->assertNull($productCategory);

    /** @var ProductCategory $childLvl1 */
    $childLvl1 = ProductCategory::query()->where('id', $childLvl1->id)->first();
    $this->assertNull($childLvl1);
});

test('POST /api/v1/contents/product-categories:mass-delete 200', function () {
    /** @var ProductCategory $productCategoryFirst */
    $productCategoryFirst = ProductCategory::factory()->create();
    /** @var ProductCategory $childLvl1 */
    $childLvl1 = ProductCategory::factory()->create(['parent_id' => $productCategoryFirst->id]);

    /** @var ProductCategory $productCategorySecond */
    $productCategorySecond = ProductCategory::factory()->create();

    postJson('/api/v1/contents/product-categories:mass-delete', [
        'ids' => [
            $productCategoryFirst->id,
            $productCategorySecond->id,
        ]
    ])
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    /** @var ProductCategory $productCategory */
    $productCategory = ProductCategory::query()->where('id', $productCategoryFirst->id)->first();
    $this->assertNull($productCategory);

    /** @var ProductCategory $childLvl1 */
    $childLvl1 = ProductCategory::query()->where('id', $childLvl1->id)->first();
    $this->assertNull($childLvl1);

    /** @var ProductCategory $productCategorySecond */
    $productCategorySecond = ProductCategory::query()->where('id', $productCategorySecond->id)->first();
    $this->assertNull($productCategorySecond);
});

test('POST /api/v1/contents/product-categories:search 200', function () {
    ProductCategory::factory()->count(5)->create();
    /** @var ProductCategory $productCategory */
    $productCategory = ProductCategory::factory()->create();
    //  ResponseBodyPagination  oneOf: 2 варианта, из-за этого валидация ответа не проходит (возможен только один вариант ответа)
    $this->skipNextOpenApiResponseValidation()
        ->postJson('/api/v1/contents/product-categories:search', [
        'filter' => [
            'name' => $productCategory->name
        ]
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.0.name', $productCategory->name);
});

test('POST /api/v1/contents/product-categories:search 400', function () {
    //  ResponseBodyPagination  oneOf: 2 варианта, из-за этого валидация ответа не проходит (возможен только один вариант ответа)
    $this->skipNextOpenApiResponseValidation()
        ->postJson('/api/v1/contents/product-categories:search', [
            'filter' => [
                'no_allowed_filter' => ''
            ]
        ])
        ->assertStatus(400);
});

test('POST /api/v1/contents/product-categories:tree 200', function () {
    /** @var ProductCategory $productCategory */
    $productCategory = ProductCategory::factory()->create();
    /** @var ProductCategory $childLvl1 */
    $childLvl1 = ProductCategory::factory()->create(['parent_id' => $productCategory->id]);

    /** @var ProductCategory $childLvl2 */
    $childLvl2 = ProductCategory::factory()->create(['parent_id' => $childLvl1->id]);

    postJson('/api/v1/contents/product-categories:tree')
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $productCategory->id)
        ->assertJsonPath('data.0.children.0.id', $childLvl1->id)
        ->assertJsonPath('data.0.children.0.children.0.id', $childLvl2->id);

    /** @var ProductCategory $productCategory */
    $productCategory = ProductCategory::query()->where('id', $productCategory->id)->first();
    $this->assertNotNull($productCategory);

    assertDatabaseHas((new ProductCategory())->getTable(), [
        "id" => $productCategory->id,
        "parent_id" => null,
    ]);

    /** @var ProductCategory $childLvl1 */
    $childLvl1 = ProductCategory::query()->where('id', $childLvl1->id)->first();
    $this->assertNotNull($childLvl1);

    assertDatabaseHas((new ProductCategory())->getTable(), [
        "id" => $childLvl1->id,
        "parent_id" => $productCategory->id,
    ]);

    /** @var ProductCategory $childLvl2 */
    $childLvl2 = ProductCategory::query()->where('id', $childLvl2->id)->first();
    $this->assertNotNull($childLvl2);

    assertDatabaseHas((new ProductCategory())->getTable(), [
        "id" => $childLvl2->id,
        "parent_id" => $childLvl1->id,
    ]);
});

