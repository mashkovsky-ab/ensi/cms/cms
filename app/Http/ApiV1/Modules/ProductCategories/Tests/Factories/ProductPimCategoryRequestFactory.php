<?php

namespace App\Http\ApiV1\Modules\ProductCategories\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class ProductPimCategoryRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'code' => $this->faker->text(20),
            'product_category_id' => $this->faker->randomNumber(),
            'filters' => [
                [
                    'code' => $this->faker->text(10),
                    'value' => $this->faker->text(10)
                ]
            ]
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
