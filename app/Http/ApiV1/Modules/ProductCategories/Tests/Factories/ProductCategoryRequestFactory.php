<?php

namespace App\Http\ApiV1\Modules\ProductCategories\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class ProductCategoryRequestFactory extends BaseApiFactory
{
    private array $pimCategories = [];

    protected function definition(): array
    {
        return [
            'name' => $this->faker->text(50),
            'url' => $this->faker->url(),
            'active' => $this->faker->boolean(),
            'order' => $this->faker->randomNumber(),
            'parent_id' => null,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray(
            array_merge(
                $extra,
                ['pim_categories' => $this->pimCategories]
            ));
    }

    public function withPimCategory($pimCategory): self
    {
        $this->pimCategories[] = $pimCategory;

        return $this;
    }
}
