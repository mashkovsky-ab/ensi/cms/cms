<?php

namespace App\Http\ApiV1\Modules\ProductCategories\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class MassDeleteProductCategoryRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'ids' => ['required', 'array'],
            'ids.*' => ['integer'],
        ];
    }

    /**
     * Возвращает идентификаторы удаляемых сущностей.
     * @return array|int[]
     */
    public function getIds(): array
    {
        return $this->validated()['ids'];
    }
}
