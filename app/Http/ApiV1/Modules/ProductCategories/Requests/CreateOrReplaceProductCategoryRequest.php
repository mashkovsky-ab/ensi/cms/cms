<?php

namespace App\Http\ApiV1\Modules\ProductCategories\Requests;

use App\Domain\Contents\Models\ProductCategory\ProductCategory;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateOrReplaceProductCategoryRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'url' => ['required', 'string'],
            'active' => ['required', 'bool'],
            'order' => ['nullable', 'integer'],
            'parent_id' => ['nullable', 'integer', Rule::exists(ProductCategory::class, 'id')],

            'pim_categories' => ['array'],
            'pim_categories.*.code' => ['required_with:pim_categories', 'string'],
            'pim_categories.*.filters' => ['array'],
            'pim_categories.*.filters.*.code' => ['string', 'required_with:pim_categories.*.filters'],
            'pim_categories.*.filters.*.value' => ['string', 'required_with:pim_categories.*.filters'],
        ];
    }
}
