<?php

namespace App\Http\ApiV1\Modules\ProductCategories\Requests;

use App\Domain\Contents\Models\ProductCategory\ProductCategory;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateOrReplaceProductPimCategoryRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'code' => ['required', 'string'],
            'product_category_id' => ['required', 'integer', Rule::exists(ProductCategory::class, 'id')],
            'filters' => ['array'],
            'filters.*.code' => ['string', 'required_with:filters'],
            'filters.*.value' => ['string', 'required_with:filters'],
        ];
    }
}
