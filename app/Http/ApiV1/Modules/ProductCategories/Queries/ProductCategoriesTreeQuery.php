<?php

namespace App\Http\ApiV1\Modules\ProductCategories\Queries;

use App\Domain\Contents\Models\ProductCategory\ProductCategory;
use Kalnoy\Nestedset\QueryBuilder as NestedSetQueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Http\Request;

class ProductCategoriesTreeQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        parent::__construct(
            ProductCategory::query()->select(['id', 'name', 'url', 'active', 'order', 'parent_id']),
            new Request($request->all())
        );

        $this->allowedSorts('id', 'name', 'url', 'order');
        $this->defaultSort('order', 'id');

        $this->allowedFilters([
            AllowedFilter::callback(
                'root_id',
                fn(NestedSetQueryBuilder $query, mixed $value) => $query->whereDescendantOrSelf($value)
            ),
            AllowedFilter::exact('active'),
        ]);
    }
}
