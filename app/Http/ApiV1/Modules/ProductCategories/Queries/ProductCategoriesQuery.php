<?php

namespace App\Http\ApiV1\Modules\ProductCategories\Queries;

use App\Domain\Contents\Models\ProductCategory\ProductCategory;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class ProductCategoriesQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        parent::__construct(ProductCategory::query(), new Request($request->all()));

        $this->allowedSorts(['id', 'name', 'url', 'parent_id', 'order']);
        $this->defaultSort('order', 'id');

        $this->allowedIncludes(
            AllowedInclude::relationship('pim_categories', 'pimCategories'),
            AllowedInclude::relationship('pim_categories.filters', 'pimCategories.filters'),
        );

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('url'),
            AllowedFilter::partial('name'),
            AllowedFilter::exact('parent_id'),
            AllowedFilter::exact('active'),
        ]);
    }
}
