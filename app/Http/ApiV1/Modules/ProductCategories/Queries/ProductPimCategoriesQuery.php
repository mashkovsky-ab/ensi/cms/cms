<?php

namespace App\Http\ApiV1\Modules\ProductCategories\Queries;

use App\Domain\Contents\Models\ProductCategory\ProductPimCategory;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ProductPimCategoriesQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        parent::__construct(ProductPimCategory::query(), new Request($request->all()));

        $this->allowedSorts(['id', 'product_category_id', 'code']);
        $this->defaultSort('id');

        $this->allowedIncludes('filters');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('product_category_id'),
            AllowedFilter::partial('code'),
        ]);
    }
}
