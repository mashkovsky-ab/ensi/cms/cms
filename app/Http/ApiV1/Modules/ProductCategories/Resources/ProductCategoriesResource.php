<?php

namespace App\Http\ApiV1\Modules\ProductCategories\Resources;

use App\Domain\Contents\Models\ProductCategory\ProductCategory;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ProductCategory
 */
class ProductCategoriesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'url' => $this->url,
            'active' => $this->active,
            'order' => $this->order,
            'parent_id' => $this->parent_id,
            'pim_categories' => ProductPimCategoriesResource::collection($this->whenLoaded('pimCategories')),
        ];
    }
}
