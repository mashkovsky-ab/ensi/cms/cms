<?php

namespace App\Http\ApiV1\Modules\ProductCategories\Resources;

use App\Domain\Contents\Models\ProductCategory\ProductPimCategory;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ProductPimCategory
 */
class ProductPimCategoriesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'product_category_id' => $this->product_category_id,
            'filters' => ProductPimCategoryFiltersResource::collection($this->whenLoaded('filters')),
        ];
    }
}
