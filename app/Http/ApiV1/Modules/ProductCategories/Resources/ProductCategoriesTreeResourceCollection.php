<?php

namespace App\Http\ApiV1\Modules\ProductCategories\Resources;

use App\Domain\Contents\Models\ProductCategory\ProductCategory;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;

class ProductCategoriesTreeResourceCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return $this->collection->keyBy('id')
            ->pipe(fn(Collection $source) => $this->buildTree($source))
            ->pipe(fn(Collection $source) => $this->mapCategories($source))
            ->toArray();
    }

    private function categoryToArray(ProductCategory $category): array
    {
        return [
            'id' => $category->id,
            'name' => $category->name,
            'url' => $category->url,
            'active' => $category->active,
            'order' => $category->order,
            'children' => $this->when(
                $category->relationLoaded('descendants'),
                fn() => $this->mapCategories($category->descendants)
            )
        ];
    }

    private function mapCategories(Collection $categories): Collection
    {
        return $categories->map(
            fn(ProductCategory $category) => $this->categoryToArray($category)
        );
    }

    private function buildTree(Collection $source): Collection
    {
        $result = new Collection();

        /** @var ProductCategory $category */
        foreach ($source as $category) {
            $parent = !$category->parent_id
                ? null
                : $source->get($category->parent_id);

            if (!$parent) {
                $result->push($category);
                continue;
            }

            if (!$parent->relationLoaded('descendants')) {
                $parent->setRelation('descendants', new Collection());
            }
            $parent->descendants->push($category);
        }

        return $result;
    }
}
