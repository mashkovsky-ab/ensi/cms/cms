<?php

namespace App\Http\ApiV1\Modules\ProductCategories\Resources;

use App\Domain\Contents\Models\ProductCategory\ProductPimCategoryFilter;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ProductPimCategoryFilter
 */
class ProductPimCategoryFiltersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'value' => $this->value,
            'product_pim_category_id' => $this->product_pim_category_id,
        ];
    }
}
