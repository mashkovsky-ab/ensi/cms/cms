<?php

namespace App\Http\ApiV1\Modules\ProductCategories\Controllers;

use App\Domain\Contents\Actions\ProductCategories\CreateProductPimCategoryAction;
use App\Domain\Contents\Actions\ProductCategories\DeleteProductPimCategoryAction;
use App\Domain\Contents\Actions\ProductCategories\ReplaceProductPimCategoryAction;
use App\Http\ApiV1\Modules\ProductCategories\Queries\ProductPimCategoriesQuery;
use App\Http\ApiV1\Modules\ProductCategories\Requests\CreateOrReplaceProductPimCategoryRequest;
use App\Http\ApiV1\Modules\ProductCategories\Resources\ProductPimCategoriesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductPimCategoriesController
{
    public function create(CreateOrReplaceProductPimCategoryRequest $request, CreateProductPimCategoryAction $action): ProductPimCategoriesResource
    {
        $dataFromFilters = $request->get('filters', []);

        return new ProductPimCategoriesResource($action->execute($request->validated(), $dataFromFilters));
    }

    public function replace(
        int $entityId,
        CreateOrReplaceProductPimCategoryRequest $request,
        ReplaceProductPimCategoryAction $action
    ): ProductPimCategoriesResource {
        $dataFromFilters = $request->get('filters', []);

        return new ProductPimCategoriesResource($action->execute($entityId, $request->validated(), $dataFromFilters));
    }

    public function delete(int $entityId, DeleteProductPimCategoryAction $action): EmptyResource
    {
        $action->execute($entityId);
        return new EmptyResource();
    }

    public function get(int $entityId, ProductPimCategoriesQuery $query): ProductPimCategoriesResource
    {
        return new ProductPimCategoriesResource($query->findOrFail($entityId));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, ProductPimCategoriesQuery $query): AnonymousResourceCollection
    {
        return ProductPimCategoriesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

}
