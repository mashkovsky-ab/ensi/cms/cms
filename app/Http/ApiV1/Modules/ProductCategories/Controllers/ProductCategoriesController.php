<?php

namespace App\Http\ApiV1\Modules\ProductCategories\Controllers;

use App\Domain\Contents\Actions\ProductCategories\CreateProductCategoryAction;
use App\Domain\Contents\Actions\ProductCategories\CreateProductPimCategoryAction;
use App\Domain\Contents\Actions\ProductCategories\DeleteProductCategoryAction;
use App\Domain\Contents\Actions\ProductCategories\MassDeleteProductCategoryAction;
use App\Domain\Contents\Actions\ProductCategories\ReplaceProductCategoryAction;
use App\Http\ApiV1\Modules\ProductCategories\Queries\ProductCategoriesQuery;
use App\Http\ApiV1\Modules\ProductCategories\Queries\ProductCategoriesTreeQuery;
use App\Http\ApiV1\Modules\ProductCategories\Requests\CreateOrReplaceProductCategoryRequest;
use App\Http\ApiV1\Modules\ProductCategories\Requests\MassDeleteProductCategoryRequest;
use App\Http\ApiV1\Modules\ProductCategories\Resources\ProductCategoriesResource;
use App\Http\ApiV1\Modules\ProductCategories\Resources\ProductCategoriesTreeResourceCollection;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductCategoriesController
{
    public function create(CreateOrReplaceProductCategoryRequest $request, CreateProductCategoryAction $action, CreateProductPimCategoryAction $productPimCategoryAction): ProductCategoriesResource
    {
        $dataFromPimCategories = $request->get('pim_categories', []);
        return new ProductCategoriesResource($action->execute($request->validated(), $dataFromPimCategories, $productPimCategoryAction));
    }

    public function replace(
        int $entityId,
        CreateOrReplaceProductCategoryRequest $request,
        ReplaceProductCategoryAction $action,
        CreateProductPimCategoryAction $productPimCategoryAction
    ): ProductCategoriesResource {
        $dataFromPimCategories = $request->get('pim_categories', []);
        return new ProductCategoriesResource($action->execute($entityId, $request->validated(), $dataFromPimCategories, $productPimCategoryAction));
    }

    public function delete(int $entityId, DeleteProductCategoryAction $action): EmptyResource
    {
        $action->execute($entityId);
        return new EmptyResource();
    }

    public function massDelete(MassDeleteProductCategoryRequest $request, MassDeleteProductCategoryAction $action): EmptyResource
    {
        $action->execute($request->getIds());
        return new EmptyResource();
    }

    public function get(int $entityId, ProductCategoriesQuery $query): ProductCategoriesResource
    {
        return new ProductCategoriesResource($query->findOrFail($entityId));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, ProductCategoriesQuery $query): AnonymousResourceCollection
    {
        return ProductCategoriesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function tree(ProductCategoriesTreeQuery $query): ProductCategoriesTreeResourceCollection
    {
        return new ProductCategoriesTreeResourceCollection($query->get());
    }
}
