<?php

namespace App\Http\ApiV1\Modules\ProductGroups\Resources;

use App\Domain\Contents\Models\ProductGroup;
use App\Http\ApiV1\Modules\Banners\Resources\BannersResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/** @mixin ProductGroup */
class ProductGroupsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'active' => $this->active,
            'is_shown' => $this->is_shown,
            'preview_photo' => $this->mapPublicFileToResponse($this->preview_photo),
            'type_id' => $this->type_id,
            'banner_id' => $this->banner_id,
            'category_code' => $this->category_code,
            'filters' => ProductGroupFiltersResource::collection($this->whenLoaded('filters')),
            'products' => ProductGroupProductsResource::collection($this->whenLoaded('products')),
            'type' => new ProductGroupTypesResource($this->whenLoaded('type')),
            'banner' => new BannersResource($this->whenLoaded('banner')),
        ];
    }
}
