<?php

namespace App\Http\ApiV1\Modules\ProductGroups\Resources;

use App\Domain\Contents\Models\ProductGroupType;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/** @mixin ProductGroupType */
class ProductGroupTypesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
        ];
    }
}
