<?php

namespace App\Http\ApiV1\Modules\ProductGroups\Resources;

use App\Domain\Contents\Models\ProductGroupFilter;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/** @mixin ProductGroupFilter */
class ProductGroupFiltersResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'value' => $this->value,
            'product_group_id' => $this->product_group_id,
        ];
    }
}
