<?php

namespace App\Http\ApiV1\Modules\ProductGroups\Resources;

use App\Domain\Contents\Models\ProductGroupProduct;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/** @mixin ProductGroupProduct */
class ProductGroupProductsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sort' => $this->sort,
            'product_group_id' => $this->product_group_id,
            'product_id' => $this->product_id,
            'group' => new ProductGroupsResource($this->whenLoaded('group')),
        ];
    }
}
