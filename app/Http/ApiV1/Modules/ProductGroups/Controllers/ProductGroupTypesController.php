<?php

namespace App\Http\ApiV1\Modules\ProductGroups\Controllers;

use App\Http\ApiV1\Modules\ProductGroups\Queries\ProductGroupTypesQuery;
use App\Http\ApiV1\Modules\ProductGroups\Resources\ProductGroupTypesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;

class ProductGroupTypesController
{
    public function search(PageBuilderFactory $pageBuilderFactory, ProductGroupTypesQuery $query)
    {
        return ProductGroupTypesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
