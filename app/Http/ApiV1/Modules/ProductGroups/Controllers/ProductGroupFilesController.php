<?php

namespace App\Http\ApiV1\Modules\ProductGroups\Controllers;

use App\Domain\Contents\Actions\DeleteProductGroupFileAction;
use App\Domain\Contents\Actions\UploadProductGroupFileAction;
use App\Http\ApiV1\Modules\ProductGroups\Requests\UploadProductGroupFileRequest;
use App\Http\ApiV1\Modules\ProductGroups\Resources\ProductGroupFilesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class ProductGroupFilesController
{
    public function upload($id, UploadProductGroupFileRequest $request, UploadProductGroupFileAction $action)
    {
        $file = $request->file('file');

        return new ProductGroupFilesResource($action->execute($id, $file));
    }

    public function delete($id, DeleteProductGroupFileAction $action)
    {
        $action->execute($id);

        return new EmptyResource();
    }
}
