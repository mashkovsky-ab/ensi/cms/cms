<?php

namespace App\Http\ApiV1\Modules\ProductGroups\Controllers;

use App\Domain\Contents\Actions\CreateProductGroupsAction;
use App\Domain\Contents\Actions\DeleteProductGroupsAction;
use App\Domain\Contents\Actions\ReplaceProductGroupsAction;
use App\Http\ApiV1\Modules\ProductGroups\Queries\ProductGroupsQuery;
use App\Http\ApiV1\Modules\ProductGroups\Requests\CreateOrReplaceProductGroupsRequest;
use App\Http\ApiV1\Modules\ProductGroups\Resources\ProductGroupsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class ProductGroupsController
{
    public function create(CreateOrReplaceProductGroupsRequest $request, CreateProductGroupsAction $action)
    {
        $dataFromFilters = $request->get('filters', []);
        $dataFromProducts = $request->get('products', []);

        return new ProductGroupsResource($action->execute($request->validated(), $dataFromFilters, $dataFromProducts));
    }

    public function update(int $id, CreateOrReplaceProductGroupsRequest $request, ReplaceProductGroupsAction $action)
    {
        $dataFromFilters = $request->get('filters', []);
        $dataFromProducts = $request->get('products', []);

        return new ProductGroupsResource($action->execute($id, $request->validated(), $dataFromFilters, $dataFromProducts));
    }

    public function delete(int $id, DeleteProductGroupsAction $action)
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, ProductGroupsQuery $query)
    {
        return ProductGroupsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(ProductGroupsQuery $query)
    {
        return ProductGroupsResource::make($query->firstOrFail());
    }
}
