<?php

namespace App\Http\ApiV1\Modules\ProductGroups\Requests;

use App\Domain\Contents\Models\Banner;
use App\Domain\Contents\Models\ProductGroupType;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateOrReplaceProductGroupsRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['string', 'required'],
            'code' => ['string', 'required'],
            'active' => ['boolean', 'required'],
            'is_shown' => ['boolean', 'required'],
            'type_id' => ['integer', 'required', Rule::exists(ProductGroupType::class, 'id')],
            'banner_id' => ['integer', 'nullable', Rule::exists(Banner::class, 'id')],
            'category_code' => ['string', 'nullable'],
            'products' => ['array'],
            'products.*.product_id' => ['integer', 'min:0', 'required_with:products'],
            'products.*.sort' => ['integer', 'min:0', 'required_with:products'],
            'filters' => ['array'],
            'filters.*.code' => ['string', 'required_with:filters'],
            'filters.*.value' => ['string', 'required_with:filters'],
        ];
    }
}
