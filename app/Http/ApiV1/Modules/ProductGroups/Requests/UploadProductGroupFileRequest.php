<?php

namespace App\Http\ApiV1\Modules\ProductGroups\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadProductGroupFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => ['required', 'image', 'max:2048'],
        ];
    }
}
