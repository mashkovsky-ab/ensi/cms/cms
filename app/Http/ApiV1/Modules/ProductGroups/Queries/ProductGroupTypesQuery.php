<?php

namespace App\Http\ApiV1\Modules\ProductGroups\Queries;

use App\Domain\Contents\Models\ProductGroupType;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ProductGroupTypesQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = ProductGroupType::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts([
            'id',
            'name',
            'code'
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('code'),
        ]);

        $this->defaultSort('id');
    }
}
