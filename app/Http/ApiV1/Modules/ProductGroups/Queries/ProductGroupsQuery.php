<?php

namespace App\Http\ApiV1\Modules\ProductGroups\Queries;

use App\Domain\Contents\Models\ProductGroup;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ProductGroupsQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = ProductGroup::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedIncludes(['filters', 'products', 'type', 'banner']);

        $this->allowedSorts([
            'id',
            'name',
            'code',
            'active',
            'is_shown'
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('code'),
            AllowedFilter::exact('active'),
            AllowedFilter::exact('is_shown'),
            AllowedFilter::exact('type_id'),
            AllowedFilter::exact('banner_id'),
            AllowedFilter::exact('category_code'),
            AllowedFilter::exact('products.product_id'),
        ]);

        $this->defaultSort('id');
    }
}
