<?php

namespace App\Http\ApiV1\Modules\Menus\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class UpdateMenuTreesRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'items' => 'array|required',
            'items.*.name' => 'string|required',
            'items.*.url' => 'string|nullable',
            'items.*.active' => 'boolean|required',
            'items.*.children' => 'array',
        ];
    }
}
