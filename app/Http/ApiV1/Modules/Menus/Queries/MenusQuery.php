<?php

namespace App\Http\ApiV1\Modules\Menus\Queries;

use App\Domain\Contents\Models\Menu;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class MenusQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = Menu::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedIncludes(['items']);

        $this->allowedSorts([
            'id',
            'code'
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('code'),
        ]);

        $this->defaultSort('id');
    }
}
