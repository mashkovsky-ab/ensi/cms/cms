<?php

namespace App\Http\ApiV1\Modules\Menus\Resources;

use App\Domain\Contents\Models\Menu;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/** @mixin Menu */
class MenusResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'items' => MenuItemsResource::collection($this->whenLoaded('items')),
            'itemsTree' => MenuTreesResource::collection($this->when($this->relationLoaded('items'), $this->getItemsTree())),
        ];
    }
}
