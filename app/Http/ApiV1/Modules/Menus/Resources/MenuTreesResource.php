<?php

namespace App\Http\ApiV1\Modules\Menus\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * @property string $name
 * @property string $url
 * @property bool $active
 * @property array $children
 */
class MenuTreesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'url' => $this->url,
            'active' => $this->active,
            'children' => self::collection($this->children),
        ];
    }
}
