<?php

namespace App\Http\ApiV1\Modules\Menus\Controllers;

use App\Domain\Contents\Actions\ReplaceMenuTreesAction;
use App\Http\ApiV1\Modules\Menus\Requests\UpdateMenuTreesRequest;
use App\Http\ApiV1\Modules\Menus\Resources\MenuTreesResource;

class MenuTreesController
{
    public function update(int $id, UpdateMenuTreesRequest $request, ReplaceMenuTreesAction $action)
    {
        return MenuTreesResource::collection($action->execute($id, $request->validated()));
    }
}
