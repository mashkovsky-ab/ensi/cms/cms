<?php

namespace App\Http\ApiV1\Modules\Landings\Controllers;

use App\Domain\Contents\Actions\ListLandingWidgetsAction;
use App\Http\ApiV1\Modules\Landings\Resources\LandingWidgetResource;
use Illuminate\Http\Request;

class LandingWidgetsController
{
    public function list(Request $request, ListLandingWidgetsAction $action)
    {
        return LandingWidgetResource::collection($action->execute());
    }
}
