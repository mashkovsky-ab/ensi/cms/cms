<?php

namespace App\Http\ApiV1\Modules\Landings\Controllers;

use App\Domain\Contents\Actions\CreateLandingsAction;
use App\Domain\Contents\Actions\DeleteLandingsAction;
use App\Domain\Contents\Actions\ReplaceLandingsAction;
use App\Http\ApiV1\Modules\Landings\Queries\LandingsQuery;
use App\Http\ApiV1\Modules\Landings\Requests\CreateOrReplaceLandingsRequest;
use App\Http\ApiV1\Modules\Landings\Resources\LandingsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class LandingsController
{
    public function create(CreateOrReplaceLandingsRequest $request, CreateLandingsAction $action)
    {
        return new LandingsResource($action->execute($request->validated()));
    }

    public function update(int $id, CreateOrReplaceLandingsRequest $request, ReplaceLandingsAction $action)
    {
        return new LandingsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteLandingsAction $action)
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, LandingsQuery $query)
    {
        return LandingsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
