<?php

namespace App\Http\ApiV1\Modules\Landings\Resources;

use App\Domain\Contents\LandingWidgets\LandingWidget;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/** @mixin LandingWidget */
class LandingWidgetResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $widgetData = $this->getWidgetData();

        return [
            "name" => $widgetData['name'],
            "widgetCode" => $widgetData['widgetCode'],
            "component" => $widgetData['component'],
            "previewBig" => $widgetData['previewBig'] ? $widgetData['previewBig']->toArray() : null,
            "previewSmall" => $widgetData['previewSmall'] ? $widgetData['previewSmall']->toArray() : null,
            "props" => $widgetData['props'],
        ];
    }
}
