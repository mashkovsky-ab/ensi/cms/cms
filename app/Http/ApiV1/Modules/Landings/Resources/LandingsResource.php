<?php

namespace App\Http\ApiV1\Modules\Landings\Resources;

use App\Domain\Contents\Models\Landing;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/** @mixin Landing */
class LandingsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'active' => $this->active,
            'code' => $this->code,
            'widgets' => $this->widgets,
        ];
    }
}
