<?php

namespace App\Http\ApiV1\Modules\Landings\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplaceLandingsRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'active' => ['boolean', 'required'],
            'name' => ['string', 'required'],
            'code' => ['string', 'required'],
            'widgets' => ['array', 'nullable'], // @todo расписать подробнее
        ];
    }
}
