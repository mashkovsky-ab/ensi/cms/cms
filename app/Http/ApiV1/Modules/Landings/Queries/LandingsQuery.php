<?php

namespace App\Http\ApiV1\Modules\Landings\Queries;

use App\Domain\Contents\Models\Landing;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class LandingsQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = Landing::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts([
            'id',
            'name',
            'active',
            'code'
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('active'),
            AllowedFilter::exact('code'),
        ]);

        $this->defaultSort('id');
    }
}
