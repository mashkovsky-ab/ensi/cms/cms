<?php

namespace App\Http\ApiV1\Modules\Banners\Queries;

use App\Domain\Contents\Models\BannerType;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class BannerTypesQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = BannerType::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts([
            'id',
            'name',
            'active',
            'code'
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('active'),
            AllowedFilter::exact('code'),
        ]);

        $this->defaultSort('id');
    }
}
