<?php

namespace App\Http\ApiV1\Modules\Banners\Queries;

use App\Domain\Contents\Models\Banner;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class BannersQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = Banner::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedIncludes(['type', 'button']);

        $this->allowedSorts([
            'id',
            'name',
            'active'
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('active'),
            AllowedFilter::exact('type_id'),
            AllowedFilter::exact('button_id'),
        ]);

        $this->defaultSort('id');
    }
}
