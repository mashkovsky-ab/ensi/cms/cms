<?php

namespace App\Http\ApiV1\Modules\Banners\Requests;

use App\Domain\Contents\Models\BannerType;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateOrReplaceBannersRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['string', 'required'],
            'active' => ['boolean', 'required'],
            'url' => ['string', 'nullable'],
            'type_id' => ['integer', 'required', Rule::exists(BannerType::class, 'id')],
            'button' => ['nullable'],
            'button.url' => ['string', 'required_with:button'],
            'button.text' => ['string', 'required_with:button'],
            'button.location' => ['string', 'required_with:button'],
            'button.type' => ['string', 'required_with:button'],
        ];
    }
}
