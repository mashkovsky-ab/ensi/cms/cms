<?php

namespace App\Http\ApiV1\Modules\Banners\Requests;

use App\Domain\Contents\Models\Banner;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DeleteBannerFileRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'type' => ['required', Rule::in(Banner::getAllImageTypes())],
        ];
    }
}
