<?php

namespace App\Http\ApiV1\Modules\Banners\Resources;

use App\Domain\Contents\Models\Banner;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/** @mixin Banner */
class BannersResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'active' => $this->active,
            'url' => $this->url,
            'desktop_image' => $this->mapPublicFileToResponse($this->desktop_image),
            'tablet_image' => $this->mapPublicFileToResponse($this->tablet_image),
            'mobile_image' => $this->mapPublicFileToResponse($this->mobile_image),
            'type_id' => $this->type_id,
            'button_id' => $this->button_id,
            'type' => new BannerTypesResource($this->whenLoaded('type')),
            'button' => new BannerButtonsResource($this->whenLoaded('button')),
        ];
    }
}
