<?php

namespace App\Http\ApiV1\Modules\Banners\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Illuminate\Http\Request;

/** @mixin EnsiFile */
class BannerFilesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource;
    }
}
