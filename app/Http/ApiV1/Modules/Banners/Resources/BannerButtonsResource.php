<?php

namespace App\Http\ApiV1\Modules\Banners\Resources;

use App\Domain\Contents\Models\BannerButton;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/** @mixin BannerButton */
class BannerButtonsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'url' => $this->url,
            'text' => $this->text,
            'location' => $this->location,
            'type' => $this->type,
        ];
    }
}
