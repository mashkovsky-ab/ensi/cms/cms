<?php

namespace App\Http\ApiV1\Modules\Banners\Controllers;

use App\Http\ApiV1\Modules\Banners\Queries\BannerTypesQuery;
use App\Http\ApiV1\Modules\Banners\Resources\BannerTypesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;

class BannerTypesController
{
    public function search(PageBuilderFactory $pageBuilderFactory, BannerTypesQuery $query)
    {
        return BannerTypesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
