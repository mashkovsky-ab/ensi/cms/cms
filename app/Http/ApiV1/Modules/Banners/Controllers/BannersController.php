<?php

namespace App\Http\ApiV1\Modules\Banners\Controllers;

use App\Domain\Contents\Actions\CreateBannersAction;
use App\Domain\Contents\Actions\DeleteBannersAction;
use App\Domain\Contents\Actions\ReplaceBannersAction;
use App\Http\ApiV1\Modules\Banners\Queries\BannersQuery;
use App\Http\ApiV1\Modules\Banners\Requests\CreateOrReplaceBannersRequest;
use App\Http\ApiV1\Modules\Banners\Resources\BannersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class BannersController
{
    public function create(CreateOrReplaceBannersRequest $request, CreateBannersAction $action)
    {
        $dataFromButton = $request->get('button');

        return new BannersResource($action->execute($request->validated(), $dataFromButton));
    }

    public function update(int $id, CreateOrReplaceBannersRequest $request, ReplaceBannersAction $action)
    {
        $dataFromButton = $request->get('button');

        return new BannersResource($action->execute($id, $request->validated(), $dataFromButton));
    }

    public function delete(int $id, DeleteBannersAction $action)
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, BannersQuery $query)
    {
        return BannersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(BannersQuery $query)
    {
        return BannersResource::make($query->firstOrFail());
    }
}
