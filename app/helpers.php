<?php

if (!function_exists('in_production')) {

    /**
     * Находится ли приложение в прод режиме
     * @return bool
     */
    function in_production()
    {
        return config('app.env', 'production') == 'production';
    }
}
