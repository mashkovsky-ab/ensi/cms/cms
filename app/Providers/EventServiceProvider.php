<?php

namespace App\Providers;

use App\Domain\Contents\Models\Banner;
use App\Domain\Contents\Models\ProductCategory\ProductPimCategory;
use App\Domain\Contents\Models\ProductGroup;
use App\Domain\Contents\Models\ProductGroupProduct;
use App\Domain\Contents\Observers\BannerObserver;
use App\Domain\Contents\Observers\ProductGroupObserver;
use App\Domain\Contents\Observers\ProductGroupProductObserver;
use App\Domain\Contents\Observers\ProductPimCategoryObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Banner::observe(BannerObserver::class);
        ProductGroup::observe(ProductGroupObserver::class);
        ProductPimCategory::observe(ProductPimCategoryObserver::class);
        ProductGroupProduct::observe(ProductGroupProductObserver::class);
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
