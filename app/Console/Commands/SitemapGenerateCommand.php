<?php

namespace App\Console\Commands;

use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Ensi\PimClient\Api\CategoriesApi as CategoryService;
use Ensi\PimClient\Api\ProductsApi as ProductService;
use Ensi\PimClient\Dto\PaginationTypeEnum;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\RequestBodyPagination;
use Ensi\PimClient\Dto\SearchCategoriesRequest;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Generator;
use Illuminate\Console\Command;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;
use SimpleXMLElement;

/**
 * Консольная команда для генерирования сайтмапа.
 *
 * Class SitemapGenerateCommand
 * @package App\Console\Commands
 */
class SitemapGenerateCommand extends Command
{
    /** @var string Базовый формат даты в сайтмапах */
    protected const DATE_FORMAT = 'Y-m-d';

    /** @var string Имя консольной команды */
    protected $signature = 'sitemap:generate';

    /** @var string Описание консольной команды */
    protected $description = 'Перегенерировать сайтмап';

    protected ProductService $productService;
    protected CategoryService $categoryService;
    protected EnsiFilesystemManager $filesystemManager;

    protected LoggerInterface $logger;

    /** @var string Домен публички сайта */
    protected string $baseDomain;

    /** @var string Путь к директории сохранения XML-файлов сайтмапов */
    protected string $xmlSaveDir;

    /**
     * SitemapGenerateCommand constructor.
     */
    public function __construct(EnsiFilesystemManager $filesystemManager)
    {
        parent::__construct();

        $this->productService = resolve(ProductService::class);
        $this->categoryService = resolve(CategoryService::class);
        $this->filesystemManager = $filesystemManager;

        $this->baseDomain = rtrim(config('app.frontend_domain'), '/') . '/';

        /** @var string xmlSaveDir */
        $this->xmlSaveDir = storage_path('sitemap/');
        if (!is_dir($this->xmlSaveDir)) {
            mkdir($this->xmlSaveDir);
        }

        $this->logger = Log::channel('sitemap:generate');
    }

    /**
     * Исполняет консольную команду.
     */
    public function handle()
    {
        ini_set('memory_limit', '10G');

        $this->generateIndexXml();

        $this->generateProductsXml();
        $this->generateCategoriesXml();
        $this->generateInnerXml();
        $this->generateImageXml();
    }

    /**
     * Генерирует sitemapindex.xml.
     * Согласно требованиям, в этот файл должны быть упакованы ссылки на отдельные файлы для товаров, категорий,
     * дополнительных страниц и картинок.
     */
    protected function generateIndexXml()
    {
        /** @var SimpleXMLElement $indexXml */
        $indexXml = $this->getBaseSitemapXmlElement('sitemapindex');

        /** @var string $lastModString */
        $lastModString = date(static::DATE_FORMAT) . 'T' . date('H:i:s') . '+03:00';

        /** @var string[] $sitemapFileNames Имена подфайлов сайтмапа */
        $sitemapFileNames = [
            'products.xml',
            'category.xml',
            'inner.xml',
            'image.xml',
        ];

        /** @var string $sitemapFileName */
        foreach ($sitemapFileNames as $sitemapFileName) {
            $sitemapTag = $indexXml->addChild('sitemap');
            $sitemapTag->addChild('loc', $this->baseDomain . 'sitemap/' . $sitemapFileName);
            $sitemapTag->addChild('lastmod', $lastModString);
        }

        /** @var string $fileName Имя файла сайтмапа */
        $fileName = 'sitemapindex.xml';
        /** @var string $savePath */
        $savePath = $this->xmlSaveDir . $fileName;

        $indexXml->asXML($savePath);
        $this->logger->info("Saved base {$fileName} file");

        $this->saveSitemap($fileName, $savePath);
    }

    /**
     * Генерирует products.xml - часть сайтмапа со ссылками на PDP.
     */
    protected function generateProductsXml(): void
    {
        $productsXml = $this->getBaseSitemapXmlElement();

        foreach ($this->iterateProducts() as $product) {
            $url = $productsXml->addChild('url');

            $url->addChild('loc', $this->buildProductUrl($product->getCode()));
            $url->addChild('lastmod', date(static::DATE_FORMAT));
            $url->addChild('changefreq', 'daily');
            $url->addChild('priority', '0.7');
        }

        $fileName = 'products.xml';
        $savePath = $this->xmlSaveDir . $fileName;

        $productsXml->asXML($savePath);
        $this->logger->info('Saved products.xml file');

        $this->saveSitemap($fileName, $savePath);
        $this->info('Finished');
    }

    /**
     * @return Generator|Product[]
     */
    private function iterateProducts(): Generator
    {
        $pagination = new RequestBodyPagination();
        $pagination->setType(PaginationTypeEnum::CURSOR);
        $pagination->setLimit(250);

        $productsRequest = new SearchProductsRequest();
        $productsRequest->setFilter((object)['allow_publish' => true]);
        $productsRequest->setPagination($pagination);

        while (true) {
            $response = $this->productService->searchProducts($productsRequest);
            yield from $response->getData();

            $nextCursor = $response->getMeta()->getPagination()?->getNextCursor();
            if ($nextCursor === null) {
                break;
            }

            $pagination->setCursor($nextCursor);
        }
    }

    /**
     * Генерирует categories.xml - часть сайтмапа со ссылками на PLP.
     */
    protected function generateCategoriesXml(): void
    {
        $categoryXml = $this->getBaseSitemapXmlElement();

        $pagination = new RequestBodyPagination();
        $pagination->setType(PaginationTypeEnum::OFFSET);
        $pagination->setLimit(-1);

        $searchCategoriesRequest = new SearchCategoriesRequest();
        $searchCategoriesRequest->setPagination($pagination);

        $categories = collect($this->categoryService->searchCategories($searchCategoriesRequest)->getData())
            ->filter(fn ($c) => !empty($c['id']))
            ->keyBy('id');

        /** @var array $category Массив с данными по категории */
        foreach ($categories as $category) {
            /** @var string $categoryCode Код категории, по которому формируется урл */
            $categoryCode = $category['code'];
            $parentCategoryCode = $category['parent_id'] && isset($categories[$category['parent_id']])
                ? $categories[$category['parent_id']]['code']
                : null;

            /** @var string $priority Показатель приоритета; 0.8 для категорий 2 уровня, 0.9 - для 1-го */
            $priority = $category['parent_id'] ? '0.8' : '0.9';

            $url = $categoryXml->addChild('url');
            $url->addChild('loc', $this->buildCategoryUrl($categoryCode, $parentCategoryCode));
            $url->addChild('lastmod', date(static::DATE_FORMAT));
            $url->addChild('changefreq', 'daily');
            $url->addChild('priority', $priority);
        }

        /** @var string $fileName Имя файла сайтмапа */
        $fileName = 'category.xml';
        /** @var string $savePath */
        $savePath = $this->xmlSaveDir . $fileName;

        $categoryXml->asXML($savePath);
        $this->logger->info("Saved {$fileName} file");

        $this->saveSitemap($fileName, $savePath);
    }

    /**
     * Генерирует inner.xml - часть сайтмапа со ссылками на внутренние страницы.
     */
    protected function generateInnerXml(): void
    {
        /** @var SimpleXMLElement $innerXml */
        $innerXml = $this->getBaseSitemapXmlElement();

        /** @var string[] $innerPages Список урлов статических, контентных и пр. внутренних страниц */
        $innerPages = [
            '' => [],
            // ...
        ];

        $defaultSetting = ['priority' => 1];

        /**
         * @var string $innerPage
         * @var array $settings
         */
        foreach ($innerPages as $innerPage => $settings) {
            $url = $innerXml->addChild('url');
            $url->addChild('loc', $this->baseDomain . $innerPage);
            $url->addChild('lastmod', date(static::DATE_FORMAT));
            $url->addChild('changefreq', 'daily');
            $url->addChild('priority', $settings['priority'] ?? $defaultSetting['priority']);
        }

        /** @var string $fileName Имя файла сайтмапа */
        $fileName = 'inner.xml';
        /** @var string $savePath */
        $savePath = $this->xmlSaveDir . $fileName;

        $innerXml->asXML($savePath);
        $this->logger->info("Saved {$fileName} file");

        $this->saveSitemap($fileName, $savePath);
    }

    /**
     * Генерирует image.xml - часть сайтмапа с изображениями.
     */
    protected function generateImageXml(): void
    {
        $imageXml = $this->getBaseSitemapXmlElement(
            'urlset',
            'xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"'
        );

        foreach ($this->iterateProducts() as $product) {
            if (blank($product->getMainImage())) {
                continue;
            }

            $url = $imageXml->addChild('url');
            $url->addChild('loc', $this->buildProductUrl($product->getCode()));

            $currentImageXml = $url->addChild('xmlns:image:image');
            $currentImageXml->addChild('xmlns:image:loc', $product->getMainImage());
            $currentImageXml->addChild('xmlns:image:title', htmlspecialchars($product->getName()));
        }

        $fileName = 'image.xml';
        $savePath = $this->xmlSaveDir . $fileName;

        $imageXml->asXML($savePath);
        $this->logger->info("Saved {$fileName} file");

        $this->saveSitemap($fileName, $savePath);
    }

    /**
     * Загружает файл сайтмапа в Ensi Storage
     */
    protected function saveSitemap(string $fileName, string $sourcePath): void
    {
        $disk = $this->filesystemManager->public();
        $disk->putFileAs('sitemap', new File($sourcePath), $fileName);
        $this->logger->info("Saved {$fileName} file to ensi storage");
    }

    /**
     * Формирует базовый XML-элемент.
     *
     * @param string $headingTag
     * @param string $additions Дополнительный атрибут или атрибуты для headingTag'а
     * @return SimpleXMLElement
     */
    protected function getBaseSitemapXmlElement(string $headingTag = 'urlset', string $additions = ''): SimpleXMLElement
    {
        return new SimpleXMLElement(
            "<?xml version='1.0' encoding='UTF-8' ?><" . $headingTag . " xmlns='http://www.sitemaps.org/schemas/sitemap/0.9' " . $additions . " />"
        );
    }

    /**
     * Строит урл категории с переданным кодом.
     *
     * @param string $categoryCode
     * @param string|null $parentCategoryCode
     * @return string
     */
    protected function buildCategoryUrl(string $categoryCode, string $parentCategoryCode = null): string
    {
        $url = $this->baseDomain . 'catalog/';
        if ($parentCategoryCode) {
            $url .= $parentCategoryCode . '/';
        }
        $url .= $categoryCode . '/';

        return $url;
    }

    /**
     * Строит урл PDP для товара с переданным кодом.
     *
     * @param string $productCode
     * @return string
     */
    protected function buildProductUrl(string $productCode): string
    {
        return $this->baseDomain . 'product/' . $productCode . '/';
    }
}
